from rest_framework import permissions
from rest_framework.generics import ListAPIView, RetrieveAPIView, UpdateAPIView, CreateAPIView
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import User
from .serializers import UserSerializer, CreateUserSerializer


class UserListView(ListAPIView):
    """
    Lists user accounts

    * only admins can access this view
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAdminUser,)


class UserDetailView(RetrieveAPIView, UpdateAPIView):
    """
    Retrieves and update user accounts

    * only admins can access this view
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAdminUser,)


class UserCreateView(CreateAPIView):
    """
    Creates user accounts

    * only admins can access this view
    """
    queryset = User.objects.all()
    serializer_class = CreateUserSerializer
    permission_classes = (IsAdminUser,)


class IsAdminView(APIView):
    """
    Convenience view to check whether the user is an admin or not.
    Returns true if the user is an admin

    * only admins can access this view
    """
    permission_classes = (permissions.IsAdminUser,)

    def get(self, request):
        return Response(True)
