from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from users.views import IsAdminView, UserCreateView, UserListView, UserDetailView

urlpatterns = [
    path('', UserListView.as_view(), name='user-list'),
    path('<uuid:pk>/', UserDetailView.as_view(), name='user-detail'),
    path('create/', UserCreateView.as_view(), name='user-create'),
    path('isadmin/', IsAdminView.as_view(), name='user-isadmin'),
]

urlpatterns = format_suffix_patterns(urlpatterns)