"""
Test image tiling operations
"""
import os
from unittest.mock import patch, call, ANY, MagicMock
from unittest import skipIf

import numpy as np

from django.conf import settings
from django.test import TestCase
from pyfakefs import fake_filesystem_unittest

from datasets.models import Dataset, Nature
from datasets.models import Point
from datasets.tiling import (TilesCreator,
                             get_coords_range_for_tiles, create_tiles_for_x_y)
from datasets.image import Tiler, create_tiles_for_zoom


def _crop_image(image, num_tiles_x, num_tiles_y, tile_size, zoom_dir,
                x_offset, y_offset, fmt='.png', pbar=None):
    for x in range(num_tiles_x):
        x_dir = os.path.join(zoom_dir, str(x_offset + x))
        if not os.path.exists(x_dir):
            os.mkdir(x_dir)
        for y in range(num_tiles_y):
            box = (x * tile_size, y * tile_size,
                   (x + 1) * tile_size, (y + 1) * tile_size)
            croped_image = image.crop(box)
            croped_image.save(os.path.join(x_dir, str(y_offset + y) + fmt))
            if pbar is not None:
                pbar.update(1)


class _Tiler(Tiler):
    def enhance_image(self):
        return self.image


class TilesCreatorTestCase(TestCase, fake_filesystem_unittest.TestCase):
    def setUp(self):
        self.dataset = Dataset.objects.create(
            label='inria', key='inria', version='1.0.0',
            metadata='{\"coords_range\":[-21, 21]}')

        self.authors = Nature.objects.create(key='authors', layer=True,
                                             dataset=self.dataset)
        self.articles = Nature.objects.create(key='articles', layer=True,
                                              dataset=self.dataset)
        self.labs = Nature.objects.create(key='labs', dataset=self.dataset)

        self.setup_es_search()
        self.setUpPyfakefs()

    def setup_es_search(self):
        patcher = patch('datasets.tiling.Search')
        self.addCleanup(patcher.stop)
        self.mock_search = MagicMock()

        p1 = Point(position=[-12.34, 45.67], score=10.0, nature='articles')
        p2 = Point(position=[34.56, -78.98], score=20.0, nature='articles')
        p3 = Point(position=[76.45, -73.48], score=30.0, nature='authors')
        p4 = Point(position=[-66.99, -39.38], score=40.0, nature='authors')

        self.mock_search.source.return_value = self.mock_search
        self.mock_search.scan.return_value = [p1, p2, p3, p4]

        self.mock_search_class = patcher.start()
        self.mock_search_class.return_value = self.mock_search

    def test_tiles_creator_construction(self):
        """
        I can create a TilesCreator object from a Dataset
        :return:
        """
        dataset = Dataset.objects.get(key=self.dataset.key)
        tiler = TilesCreator(dataset)
        self.assertDictEqual(tiler.metadata, {'coords_range': [-21, 21]})

    def test_tiles_creator_create_dataframe(self):
        """
        TilesCreator.create_dataframe should create a pandas dataframe with
        the right columns.
        :return:
        """
        dataset = Dataset.objects.get(key=self.dataset.key)
        tiler = TilesCreator(dataset)
        df = tiler.create_dataframe()
        self.mock_search_class.assert_called_with(
            index=self.dataset.key + '-' + self.dataset.version,
            doc_type=Point
        )
        self.mock_search.source.assert_called_with([
            'position', 'score', 'nature'
        ])
        self.assertEquals(df.shape, (4, 4))
        self.assertEquals(list(df), ['score', 'nature', 'x_pos', 'y_pos'])

    @patch('datasets.tiling.tqdm')
    @patch('datasets.tiling.create_tiles_for_zoom')
    def test_tiles_creator_create_tiles(self, mock_create_tiles_for_zoom,
                                        mocktqdm):
        """
        TilesCreator.create_tiles should clear the tiling dir if it exists and
        call the function create_tiles_for_zoom for each nature and zoom level
        :param mock_create_tiles:
        :return:
        """
        dataset = Dataset.objects.get(key=self.dataset.key)
        tiler = TilesCreator(dataset)
        tiles_dir = os.path.join(
            settings.STATIC_ROOT,
            'tiles/{}/{}'.format(dataset.key, dataset.version)
        )
        self.fs.create_dir(tiles_dir)
        self.fs.create_file(os.path.join(tiles_dir, 'tmp.txt'))
        max_zoom = 3

        tiler.create_tiles(max_zoom)

        calls = [call(ANY, 256, max_zoom, os.path.join(tiles_dir, 'authors'),
                      [-21, 21], ANY, ANY),
                 call(ANY, 256, max_zoom, os.path.join(tiles_dir, 'articles'),
                      [-21, 21], ANY, ANY)]
        mock_create_tiles_for_zoom.assert_has_calls(calls)

        # assert old file was removed
        self.assertFalse(os.path.exists(os.path.join(tiles_dir, 'tmp.txt')))
        # assert nature dirs were created
        self.assertTrue(os.path.isdir(os.path.join(tiles_dir, 'authors')))
        self.assertTrue(os.path.isdir(os.path.join(tiles_dir, 'articles')))

    @patch('datasets.image._crop_save')
    def test_create_tiles_for_zoom(self, mock_crop_save):
        """
        create_tiles_for_zoom function should create a full size image and
        then tile it.
        :param mock_create_image:
        :return:
        """
        dataset = Dataset.objects.get(key=self.dataset.key)
        tiler = TilesCreator(dataset)
        df = tiler.create_dataframe()
        zoom = 5
        # nb_tiles = pow(2, zoom)
        coords_range = [-100, 100]
        tiles_dir = os.path.join(
            settings.STATIC_ROOT,
            'tiles/{}/{}'.format(dataset.key, dataset.version)
        )
        self.fs.create_dir(tiles_dir)
        create_tiles_for_zoom(df, 256, zoom, tiles_dir, coords_range)

        calls = []
        # Assert that it created a dir for the zoom
        for z in reversed(range(zoom)):
            self.assertTrue(os.path.isdir(os.path.join(tiles_dir, str(z))))
            num_tiles = 2**z
            for x in range(num_tiles):
                for y in range(num_tiles):
                    calls.append(
                        call(ANY, ANY,
                             os.path.join(tiles_dir, str(z),
                                          str(x), '{}.png'.format(y))))

        mock_crop_save.assert_has_calls(calls)

    @skipIf(os.environ.get('CI_TEST', ''), "Too expensive for CI.")
    @patch('datasets.image._crop_save')
    def test_create_tiles_for_zoom_7(self, mock_crop_save):
        """
        create_tiles_for_zoom function should create a medium sized image for
        zoom > 5 and then tile it.
        :param mock_crop_image:
        :param mock_create_image:
        :param mock_export_image:
        :return:
        """

        dataset = Dataset.objects.get(key=self.dataset.key)
        tiler = TilesCreator(dataset)
        df = tiler.create_dataframe()
        zoom = 7
        coords_range = [-100, 100]
        tiles_dir = os.path.join(settings.STATIC_ROOT,
                                 'tiles/{}'.format(dataset.key))
        self.fs.create_dir(tiles_dir)
        create_tiles_for_zoom(df, 256, zoom, tiles_dir, coords_range)

        calls = []
        # Assert that it created a dir for the zoom
        for z in reversed(range(zoom)):
            self.assertTrue(os.path.isdir(os.path.join(tiles_dir, str(z))))
            num_tiles = 2**z
            for x in range(num_tiles):
                for y in range(num_tiles):
                    calls.append(
                        call(ANY, ANY,
                             os.path.join(tiles_dir, str(z),
                                          str(x), '{}.png'.format(y))))

        mock_crop_save.assert_has_calls(calls, any_order=True)

    def test_crop_image_zoom_0(self):
        """
        crop_image should work for zoom = 0
        :return:
        """
        dataset = Dataset.objects.get(key=self.dataset.key)
        tiles_dir = os.path.join(settings.STATIC_ROOT,
                                 'tiles/{}/0'.format(dataset.key))
        self.fs.create_dir(tiles_dir)
        mock_img = MagicMock()
        mock_img.crop.return_value = mock_img

        _crop_image(mock_img, 1, 1, 256, tiles_dir, 0, 0)

        self.assertTrue(os.path.isdir(os.path.join(tiles_dir, '0')))
        mock_img.crop.assert_called_with((0, 0, 256, 256))
        mock_img.save.assert_called_with(os.path.join(tiles_dir, '0/0.png'))

    def test_crop_image(self):
        """
        crop_image should create tiles and save them
        :return:
        """
        dataset = Dataset.objects.get(key=self.dataset.key)
        tiles_dir = os.path.join(settings.STATIC_ROOT,
                                 'tiles/{}/0'.format(dataset.key))
        self.fs.create_dir(tiles_dir)
        mock_img = MagicMock()
        mock_crop = MagicMock()
        mock_img.crop.return_value = mock_crop
        zoom = 3
        num_tiles = pow(2, zoom)
        tile_size = 256
        img_size = num_tiles * tile_size

        tiler = _Tiler(img_size, img_size, None, None)
        tiler.max = 0
        tiler.image = mock_img
        tiler.crop_image(num_tiles, num_tiles, tile_size, tiles_dir, 0, 0)

        # crop_image(mock_img, num_tiles, num_tiles, 256, tiles_dir, 0, 0)

        # Assert that it creates all x dirs
        for x in range(num_tiles):
            self.assertTrue(os.path.isdir(os.path.join(tiles_dir, str(x))))

        # Assert that it cropped and saved images
        calls = []
        for x in range(num_tiles):
            for y in range(num_tiles):
                calls.append(call(os.path.join(tiles_dir,
                                               '{}/{}.png'.format(x, y))))
        mock_crop.save.assert_has_calls(calls)

    def test_crop_image_with_offset(self):
        """
        crop_image should create tiles and save them
        :return:
        """
        dataset = Dataset.objects.get(key=self.dataset.key)
        tiles_dir = os.path.join(settings.STATIC_ROOT,
                                 'tiles/{}/0'.format(dataset.key))
        self.fs.create_dir(tiles_dir)
        mock_img = MagicMock()
        mock_crop = MagicMock()
        zoom = 3
        num_tiles = pow(2, zoom)
        offset = 32
        tile_size = 256
        img_size = num_tiles * tile_size
        mock_img.size = (tile_size, tile_size)
        mock_img.dtype = np.uint8
        mock_img.crop.return_value = mock_crop

        tiler = _Tiler(img_size, img_size, None, None)
        tiler.max = 0
        tiler.image = mock_img
        tiler.crop_image(num_tiles, num_tiles, tile_size, tiles_dir,
                         offset, offset)

        # Assert that it creates all x dirs
        for x in range(num_tiles):
            self.assertTrue(os.path.isdir(os.path.join(tiles_dir,
                                                       str(offset + x))))

        # Assert that it cropped and saved images
        calls = []
        for x in range(num_tiles):
            for y in range(num_tiles):
                calls.append(call(os.path.join(
                    tiles_dir,
                    '{}/{}.png'.format(x + offset, y + offset))))
        mock_crop.save.assert_has_calls(calls)

    def test_get_coords_range_for_tiles(self):
        coords_range = [-20, 20]
        (x, y, z, nbx, nby) = (2, 10, 4, 10, 1)
        xrange, yrange = get_coords_range_for_tiles(coords_range,
                                                    x, y, z,
                                                    nbx, nby)
        self.assertEqual(xrange, (-15.0, 10.0))
        self.assertEqual(yrange, (-7.5, -5.0))

    @patch.object(Tiler, 'crop_image')
    def test_create_tiles_for_x_y(self, mock_crop):
        dataset = Dataset.objects.get(key=self.dataset.key)
        tiler = TilesCreator(dataset)
        df = tiler.create_dataframe()
        tiles_dir = os.path.join(settings.STATIC_ROOT,
                                 'tiles/{}/0'.format(dataset.key))
        self.fs.create_dir(tiles_dir)

        coords_range = [-20, 20]
        (tile_size, zoom, x, y, nb_tiles_x, nb_tiles_y) = (256, 4, 2, 10, 10, 1)
        create_tiles_for_x_y(df, tile_size, zoom,
                             x, y, nb_tiles_x, nb_tiles_y,
                             tiles_dir, coords_range)
        mock_crop.assert_called_with(nb_tiles_x, nb_tiles_y,
                                     tile_size, tiles_dir, x, y)

        create_tiles_for_x_y(df, tile_size, zoom, x, y,
                             nb_tiles_x, nb_tiles_y, tiles_dir, coords_range)
        mock_crop.assert_called_with(nb_tiles_x, nb_tiles_y,
                                     tile_size, tiles_dir, x, y)
