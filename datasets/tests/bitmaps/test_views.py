import os
import random
import string
import time
import urllib.parse
from unittest.mock import patch

import numpy as np

from django.conf import settings
from django.test import TestCase
from django.urls import reverse
from elasticsearch_dsl import Index
from pyfakefs import fake_filesystem_unittest
from rest_framework import status
from rest_framework.test import APITestCase

from datasets.bitmaps.bitmaps import GROUP_FILTER_INDEX_SUFFIX
from datasets.bitmaps.models import Group, FilterValue
from datasets.bitmaps.views import check_files
from datasets.models import Dataset


class CheckFilesTestCase(TestCase, fake_filesystem_unittest.TestCase):
    def setUp(self):
        self.setUpPyfakefs()

    def test_check_files_all_files(self):
        """
        check_files method should return a dict of layer -> np.array, where each array is all ones if all files exist
        for the layer
        :return:
        """
        uuid = 'abc-def'
        z = 3
        layers = ['words', 'teams']
        tiles_dir = os.path.join(settings.STATIC_ROOT,
                                 'grouptiles/{}/{}'.format(uuid, layers[0]))
        for x in range(8):
            for y in range(2, 6):
                self.fs.create_file(os.path.join(
                    tiles_dir, str(z), str(x), '%d.png' % y))

        to_tile, all_done = check_files(uuid, 0, 2, 3, 7, 5, layers)
        self.assertEquals(len(all_done), 1)
        self.assertEquals(len(to_tile), 1)
        self.assertTrue(np.all(all_done[layers[0]] == 1))
        self.assertTrue(np.all(to_tile[layers[1]] == 0))

    def test_check_files(self):
        """
        check_files method should return a dict of layer -> np.array, where each array is has ones where files already exist
        :return:
        """
        uuid = 'abc-def'
        z = 3
        layers = ['words', 'teams']
        tiles_dir = os.path.join(settings.STATIC_ROOT,
                                 'grouptiles/{}/{}'.format(uuid, layers[0]))
        for x in range(4):
            for y in range(2, 6):
                self.fs.create_file(os.path.join(
                    tiles_dir, str(z), str(x), '%d.png' % y))

        to_tile, all_done = check_files(uuid, 0, 2, 3, 7, 5, layers)
        self.assertEquals(len(all_done), 0)
        self.assertEquals(len(to_tile), 2)
        self.assertTrue(np.all(to_tile[layers[1]] == 0))
        self.assertTrue(np.all(to_tile[layers[0]][:4, :] == 1))
        self.assertTrue(np.all(to_tile[layers[0]][4:, :] == 0))


class DatasetGroupDetailViewTestCase(APITestCase):
    def setUp(self):
        self.dataset = Dataset.objects.create(
            label="inria", key="inria", version="1.0.0"
        )

    def test_bitmaps_required(self):
        """
        View should return 400 if bitmaps query param not provided
        :return:
        """
        url = reverse('dataset-group-detail', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version
        })
        response = self.client.get(url)
        self.assertEqual(response.status_code, 400)

    def test_query_required(self):
        """
        View should return 400 if bitmaps query param is provided but query param is not
        :return:
        """
        bitmaps = '12|34&400'
        url = reverse('dataset-group-detail', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version
        })
        url += '?bitmaps=%s' % urllib.parse.quote_plus(bitmaps)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 400)

    def test_view_creates_group(self):
        """
        View should create a new group if one does not exist
        :return:
        """
        bitmaps = '12|34&400'
        query = 'ID_labs=9375,4578&IN_year=2012'
        url = reverse('dataset-group-detail', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version
        })
        url += '?bitmaps=%s' % urllib.parse.quote_plus(bitmaps)
        url += '&query=%s' % urllib.parse.quote_plus(query)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['bitmaps'], bitmaps)
        self.assertEqual(response.data['dataset'], self.dataset.id)
        self.assertEqual(response.data['query'], query)
        self.assertIsNotNone(response.data['uuid'])

    def test_view_returns_group_if_exists(self):
        bitmaps = '34&400'
        group = Group.objects.create(dataset=self.dataset,
                                     version=self.dataset.version,
                                     bitmaps=bitmaps)

        url = reverse('dataset-group-detail', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version
        })
        url += '?bitmaps=%s' % urllib.parse.quote_plus(bitmaps)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['uuid'], str(group.uuid))
        self.assertEqual(response.data['id'], group.id)


class FilterValueListTestCase(APITestCase):
    dataset = None
    index = None

    @classmethod
    def setUpClass(cls):
        key = 'mockds' + ''.join(random.choices(string.ascii_lowercase, k=5))
        cls.dataset = Dataset.objects.create(label='inria', key=key,
                                             version="1.0.0")
        cls.create_mock_filter_values()

    @classmethod
    def tearDownClass(cls):
        cls.index.delete(ignore=404)

    @classmethod
    def create_mock_filter_values(cls):
        idx = (cls.dataset.key + "-" + cls.dataset.version +
               GROUP_FILTER_INDEX_SUFFIX)
        cls.index = Index(idx)
        cls.index.document(FilterValue)
        cls.index.delete(ignore=404)
        cls.index.create()
        cls.v1 = FilterValue(field='year', value=2010, bitmap_index=14)
        cls.v1.save(index=idx)
        cls.v2 = FilterValue(field='year', value=2007, bitmap_index=16)
        cls.v2.save(index=idx)
        cls.v3 = FilterValue(field='struct', value='Inria', bitmap_index=31)
        cls.v3.save(index=idx)
        cls.v4 = FilterValue(field='struct', value='CNRS', bitmap_index=52)
        cls.v4.save(index=idx)
        time.sleep(2)

    def test_filter_list_should_be_paginated(self):
        """
        The list of points returned by the view should be paginated.
        :return:
        """
        url = reverse('filter-values', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version
        })
        url += '?limit=2'
        print(url)

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['Content-Range'], 'items 0-2/4')
        self.assertEqual(len(response.data), 2)

    def test_filter_list_can_search_in_field(self):
        """
        View should return the list of points matching filters
        :return:
        """
        url = reverse('filter-values', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version
        })
        url += '?year=2010'
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0], doc_to_dict(self.v1))

        url = reverse('filter-values', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version
        })
        url += '?struct=cnrs'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0], doc_to_dict(self.v4))

        url = reverse('filter-values', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version
        })
        url += '?bitmap=16'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0], doc_to_dict(self.v2))


def doc_to_dict(doc):
    point_dict = doc.to_dict()
    point_dict['id'] = doc.meta.id
    return point_dict


class GroupTileViewTestCase(APITestCase):
    def setUp(self):
        self.dataset = Dataset.objects.create(label="inria", key="inria")
        self.group = Group.objects.create(
            dataset=self.dataset, bitmaps='34&400')

    def test_view_raises_errors_if_params_invalid(self):
        """
        View should return 400 if xend < x or yend < y or layers not passed
        """
        url = reverse('group-tile', kwargs={'uuid': str(
            self.group.uuid), 'z': 5, 'x': 0, 'y': 2, 'xend': 1, 'yend': 1})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 400)

        url = reverse('group-tile', kwargs={'uuid': str(
            self.group.uuid), 'z': 3, 'x': 4, 'y': 2, 'xend': 1, 'yend': 5})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 400)

        url = reverse('group-tile', kwargs={'uuid': str(
            self.group.uuid), 'z': 3, 'x': 4, 'y': 2, 'xend': 7, 'yend': 5})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 400)

    @patch('datasets.bitmaps.views.check_files')
    @patch('datasets.bitmaps.views.group_tiling_task')
    def test_view_checks_all_tiles(self, mock_tiling_task, mock_check_files):
        """
        If all files already exist, view should not call tiling task
        """
        layers = ['articles', 'authors']
        tiles = {layer: np.ones((3, 5)) for layer in layers}
        mock_check_files.return_value = {}, tiles

        url = reverse('group-tile', kwargs={'uuid': str(
            self.group.uuid), 'z': 3, 'x': 0, 'y': 2, 'xend': 2, 'yend': 6})
        url += '?layers=%s' % ','.join(layers)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.data, {layer: t.tolist() for layer, t in tiles.items()})
        mock_check_files.assert_called_with(
            self.group.uuid, 0, 2, 3, 2, 6, layers)
        mock_tiling_task.assert_not_called()

    @patch('datasets.bitmaps.views.check_files')
    @patch('datasets.bitmaps.views.group_tiling_task')
    def test_view_calls_tiling_task(self, mock_tiling_task, mock_check_files):
        """
        Tiling task should be called for layer where tiles are not all ones
        """
        layers = ['articles', 'authors']
        tiles = {'articles': np.ones((3, 5))}
        tiles['articles'][2, 3] = 0
        mock_check_files.return_value = tiles, {'authors': np.ones((3, 5))}
        mock_tiling_task.return_value = tiles

        url = reverse('group-tile', kwargs={'uuid': str(
            self.group.uuid), 'z': 3, 'x': 0, 'y': 2, 'xend': 2, 'yend': 6})
        url += '?layers=%s' % ','.join(layers)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data, {layer: tiles.get(
            layer, np.ones((3, 5))).tolist() for layer in layers})
        mock_check_files.assert_called_with(
            self.group.uuid, 0, 2, 3, 2, 6, layers)
        mock_tiling_task.assert_called_with(self.group.uuid, 0, 2, 3, tiles)
