from django.urls import reverse
from rest_framework.test import APITestCase

from datasets.models import Dataset


class DatasetViewsTestCase(APITestCase):
    def setUp(self):
        self.dataset = Dataset.objects.create(label="inria", key="inria")

    def test_dataset_detail_vew(self):
        """
        I can get a dataset by id
        :return:
        """
        url = reverse('dataset-detail', kwargs={'pk': self.dataset.pk})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], self.dataset.pk)
        self.assertEqual(response.data['label'], self.dataset.label)
        self.assertEqual(response.data['key'], self.dataset.key)

        url = reverse('dataset-detail', kwargs={'pk': 1234556})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 404)

    def test_dataset_detail_key_view(self):
        """
        I can get a dataset by key
        :return:
        """
        url = reverse('dataset-detail-key', kwargs={'key': self.dataset.key})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], self.dataset.pk)
        self.assertEqual(response.data['label'], self.dataset.label)
        self.assertEqual(response.data['key'], self.dataset.key)

        url = reverse('dataset-detail-key', kwargs={'key': 'poiuqdfmklazern'})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 404)
