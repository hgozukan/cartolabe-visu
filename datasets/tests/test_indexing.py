import json
import logging
import os
from unittest.mock import patch, MagicMock, call, ANY

import pandas as pd
from django.conf import settings
from django.test import TestCase
from pyfakefs import fake_filesystem_unittest

from datasets.bitmaps.bitmaps import (
    BitmapStore, CoordBitmapStore, COORDS_BITMAPS_FILENAME
)
from datasets.bitmaps.models import GroupFilter
from datasets.indexing import (
    ESJsonImporter, import_for_dataset, AbstractESImporter, BitmapManager,
    ESFeatherImporter
)
from datasets.models import Dataset, Nature
from datasets.models import Point


def notqdm(it, *a, **k):
    return it


class AbstractESImporterTestCase(TestCase):
    def setUp(self):
        logging.disable(logging.INFO)
        self.dataset = Dataset.objects.create(label='inria', key='inria',
                                              version='1.0.0')
        self.authors = Nature.objects.create(
            key='authors', layer=True, dataset=self.dataset
        )
        self.articles = Nature.objects.create(
            key='articles', layer=True, dataset=self.dataset
        )
        self.labs = Nature.objects.create(key='labs', dataset=self.dataset)

    def tearDown(self):
        Dataset.objects.all().delete()

    @patch.object(AbstractESImporter, '__abstractmethods__', new_callable=set)
    @patch('datasets.indexing.BitmapManager')
    def test_constructor(self, mock_bm_class, _):
        mock_bm = MagicMock()
        mock_bm_class.return_value = mock_bm
        indexer = AbstractESImporter('export.abstract', self.dataset)
        self.assertEquals(indexer.export_file, 'export.abstract')
        self.assertEquals(indexer.max_coord, 0.0)
        self.assertEquals(indexer.doc_ids, [])
        self.assertEquals(indexer.natures, ['authors', 'articles', 'labs'])
        self.assertEquals(indexer.bitmap_creator, mock_bm)

    @patch('datasets.indexing.ESFeatherImporter')
    @patch('datasets.indexing.ESJsonImporter')
    def test_import_method(self, mock_json_class, mock_feather_class):
        export_file = 'export.json'
        importer = MagicMock(spec=AbstractESImporter)
        importer.save_to_es.return_value = 25
        mock_json_class.return_value = importer

        res = import_for_dataset(export_file, self.dataset)
        self.assertEquals(res, 25)
        mock_json_class.assert_called_with(export_file, self.dataset)
        importer.save_to_es.assert_called()

        export_file = 'export.feather'
        mock_feather_class.return_value = importer
        importer.reset_mock()
        res = import_for_dataset(export_file, self.dataset)
        self.assertEquals(res, 25)
        mock_feather_class.assert_called_with(export_file, self.dataset)
        importer.save_to_es.assert_called()

    @patch('datasets.indexing.tqdm', notqdm)
    @patch.object(AbstractESImporter, '__abstractmethods__', new_callable=set)
    @patch('datasets.indexing.connections.get_connection')
    @patch('datasets.indexing.streaming_bulk')
    def test_create_points(self, mock_bulk, mock_connection_mtd, _):
        indexer = AbstractESImporter('export.abstract', self.dataset)
        mock_connection = MagicMock()
        mock_connection_mtd.return_value = mock_connection
        mock_points = MagicMock()
        indexer._gen_points = MagicMock(return_value=mock_points)
        mock_bulk.return_value = [(True, {'index': {'_id': 'abcd'}}),
                                  (True, {'index': {'_id': 'efgh'}})]

        indexer.create_points()
        mock_bulk.assert_called_with(
            mock_connection, mock_points, chunk_size=50, max_retries=20,
            initial_backoff=20, max_backoff=360)

        self.assertEquals(indexer.doc_ids, ['abcd', 'efgh'])

    @patch('datasets.indexing.tqdm', notqdm)
    @patch.object(AbstractESImporter, '__abstractmethods__', new_callable=set)
    @patch('datasets.indexing.connections.get_connection')
    @patch('datasets.indexing.streaming_bulk')
    def test_add_neighbors(self, mock_bulk, mock_connection_mtd, _):
        indexer = AbstractESImporter('export.abstract', self.dataset)
        mock_connection = MagicMock()
        mock_connection_mtd.return_value = mock_connection
        mock_points = MagicMock()
        indexer._gen_neighbors_actions = MagicMock(return_value=mock_points)
        mock_bulk.return_value = [(True, {'update': {'_id': 'abcd'}}),
                                  (True, {'update': {'_id': 'efgh'}})]

        indexer.add_neighbors()
        mock_bulk.assert_called_with(
            mock_connection, mock_points, chunk_size=50, max_retries=20,
            initial_backoff=20, max_backoff=360)

    @patch.object(AbstractESImporter, '__abstractmethods__', new_callable=set)
    @patch('datasets.indexing.Index')
    def test_save_to_es(self, mock_index_cls, _):
        indexer = AbstractESImporter('export.abstract', self.dataset)
        mock_index = MagicMock()
        mock_index_cls.return_value = mock_index

        indexer.create_points = MagicMock()
        indexer.add_neighbors = MagicMock()

        indexer.save_to_es()
        mock_index_cls.assert_called_with(
            self.dataset.key + "-" + self.dataset.version
        )
        mock_index.document.assert_called_with(Point)
        mock_index.delete.assert_called_with(ignore=404)
        mock_index.create.assert_called_with()
        indexer.create_points.assert_called_once()
        indexer.add_neighbors.assert_called_once()


class ESJsonImporterTestCase(TestCase, fake_filesystem_unittest.TestCase):

    @patch('datasets.indexing.BitmapManager')
    def setUp(self, mock_bm_class):
        logging.disable(logging.INFO)
        self.dataset = Dataset.objects.create(label='inria', key='inria',
                                              version='1.0.0')
        self.authors = Nature.objects.create(
            key='authors', layer=True, dataset=self.dataset
        )
        self.articles = Nature.objects.create(
            key='articles', layer=True, dataset=self.dataset
        )
        self.labs = Nature.objects.create(key='labs', dataset=self.dataset)
        self.json_data = [{
            'position': [-33.843028052831194, -32.17953511388667],
            'score': 220.0,
            'nature': 'words',
            'label': 'energy',
            'rank': 0,
            'neighbors': {
                'authors': [2741, 2707],
                'words': [0, 403],
                'articles': [3847, 4913, 4117, 3566, 4313,
                             3758, 3832, 3838, 3234, 4836]
            }
        }, {
            'position': [29.52342320407473, -20.52766974206584],
            'score': 208.0,
            'nature': 'articles',
            'label': 'Article topic',
            'rank': 1,
            'neighbors': {
                'll_clusters': [5521, 5528],
                'authors': [2648, 2653]
            }
        }, {
            'position': [-17.558202240998916, 51.48573771757],
            'score': 206.0,
            'nature': 'authors',
            'label': 'Jean Michel',
            'rank': 2,
            'neighbors': {
                'articles': [4538, 5048],
                'labs': [5428, 5341]
            }
        }, {
            'position': [3.0866601708015726, -90.9953497641929],
            'score': 198.0,
            'nature': 'labs',
            'label': 'CNRS',
            'rank': 3,
            'neighbors': {
                'hl_clusters': [5439, 5447],
                'authors': [2656, 2676],
                'words': [516, 3, 1568],
                'articles': [3305, 4689],
                'labs': [5206, 5236]
            }
        }]
        self.mock_bm = MagicMock(spec=BitmapManager)
        mock_bm_class.return_value = self.mock_bm
        self.setUpPyfakefs()

        export_file = 'export.json'
        self.fs.create_file(export_file, contents=json.dumps(self.json_data))
        self.indexer = ESJsonImporter(export_file, self.dataset)

    def tearDown(self):
        Dataset.objects.all().delete()

    def test_gen_points(self):
        points = self.indexer._gen_points()
        for id, data in enumerate(self.json_data):
            p = next(points)
            self.assertEquals(p['_index'],
                              self.dataset.key + "-" + self.dataset.version
                              )
            source = p['_source']
            for key in ['position', 'score', 'nature', 'label', 'rank']:
                self.assertEquals(source[key], data[key])

        self.assertEquals(self.indexer.max_coord, 90.9953497641929)

        calls = [call(idx, ANY) for idx, point in enumerate(self.json_data)]
        self.mock_bm.add_point_field_values.assert_has_calls(calls)

    def test_gen_neighbors_actions(self):
        # add fake es ids for neighbors
        self.indexer.doc_ids = [chr(x) for x in range(6000)]
        actions = self.indexer._gen_neighbors_actions()
        natures = ['authors', 'articles', 'labs']
        for id, data in enumerate(self.json_data):
            a = next(actions)
            self.assertEquals(a['_index'],
                              self.dataset.key + "-" + self.dataset.version
                              )
            self.assertEquals(a['_op_type'], 'update')
            self.assertEquals(a['_id'], self.indexer.doc_ids[int(id)])
            neighbors = []
            for nature, ids in data['neighbors'].items():
                if nature in natures:
                    neighbors.append({'nature': nature, 'ids': [
                                     self.indexer.doc_ids[id] for id in ids]})
            self.assertEquals(a['doc'], {'neighbors': neighbors})

    def test_get_element_value_function(self):
        self.assertEquals(self.indexer.get_element_value(
            0, 'label'), self.json_data[0]['label']
        )
        self.assertEquals(self.indexer.get_element_value(
            1, 'label'), self.json_data[1]['label']
        )
        self.assertEquals(self.indexer.get_element_value(
            2, 'score'), self.json_data[2]['score']
        )


class ESFeatherImporterTestCase(TestCase):

    @patch('datasets.indexing.BitmapManager')
    @patch('datasets.indexing.pd.read_feather')
    def setUp(self, mock_read, mock_bm_class):
        logging.disable(logging.INFO)
        self.dataset = Dataset.objects.create(label='inria', key='inria',
                                              version='1.0.0')
        self.authors = Nature.objects.create(
            key='authors', layer=True, dataset=self.dataset)
        self.articles = Nature.objects.create(
            key='articles', layer=True, dataset=self.dataset)
        self.labs = Nature.objects.create(key='labs', dataset=self.dataset)
        self.json_data = [{
            'x': -33.843028052831194,
            'y': -32.17953511388667,
            'score': 220.0,
            'nature': 'words',
            'label': 'energy',
            'rank': 0,
            'nn_authors': '2741,2707',
            'nn_words': '0,403',
            'nn_articles': '3847,4913,4117,3566,4313,3758,3832,3838,3234,4836'
        }, {
            'x': 29.52342320407473,
            'y': -20.52766974206584,
            'score': 208.0,
            'nature': 'articles',
            'label': 'Article topic',
            'rank': 1,
            'nn_ll_clusters': '5521,5528',
            'nn_authors': '2648,2653'
        }, {
            'x': -17.558202240998916,
            'y': 51.48573771757,
            'score': 206.0,
            'nature': 'authors',
            'label': 'Jean Michel',
            'rank': 2,
            'nn_articles': '4538,5048',
            'nn_labs': '5428,5341'
        }, {
            'x': 3.0866601708015726,
            'y': -90.9953497641929,
            'score': 198.0,
            'nature': 'labs',
            'label': 'CNRS',
            'rank': 3,
            'nn_hl_clusters': '5439,5447',
            'nn_authors': '2656,2676',
            'nn_words': '516,3,1568',
            'nn_articles': '3305,4689',
            'nn_labs': '5206,5236'
        }]
        self.mock_bm = MagicMock(spec=BitmapManager)
        mock_bm_class.return_value = self.mock_bm
        df = pd.DataFrame(self.json_data)
        df = df.where((pd.notnull(df)), None)
        mock_read.return_value = df

        export_file = 'export.feather'
        self.indexer = ESFeatherImporter(export_file, self.dataset)

    def tearDown(self):
        Dataset.objects.all().delete()

    def test_gen_points(self):
        points = self.indexer._gen_points()
        for id, data in enumerate(self.json_data):
            p = next(points)
            self.assertEquals(p['_index'],
                              self.dataset.key + '-' + self.dataset.version)
            source = p['_source']
            for key in ['score', 'nature', 'label', 'rank']:
                self.assertEquals(source[key], data[key])
            self.assertEquals(source['position'], [data['x'], data['y']])

        self.assertEquals(self.indexer.max_coord, 90.9953497641929)

        calls = [call(idx, ANY) for idx, point in enumerate(self.json_data)]
        self.mock_bm.add_point_field_values.assert_has_calls(calls)

    def test_gen_neighbors_actions(self):
        # add fake es ids for neighbors
        self.indexer.doc_ids = [chr(x) for x in range(6000)]
        actions = self.indexer._gen_neighbors_actions()
        natures = ['authors', 'articles', 'labs']
        for id, data in enumerate(self.json_data):
            a = next(actions)
            self.assertEquals(a['_index'],
                              self.dataset.key + '-' + self.dataset.version)
            self.assertEquals(a['_op_type'], 'update')
            self.assertEquals(a['_id'], self.indexer.doc_ids[int(id)])
            neighbors = []
            for nature in natures:
                if 'nn_' + nature in data:
                    neighbors.append({
                        'nature': nature,
                        'ids': [self.indexer.doc_ids[int(id)]
                                for id in data['nn_' + nature].split(',')]
                    })
            self.assertEquals(a['doc'], {'neighbors': neighbors})

    def test_get_element_value_function(self):
        self.assertEquals(self.indexer.get_element_value(
            0, 'label'), self.json_data[0]['label']
        )
        self.assertEquals(self.indexer.get_element_value(
            1, 'label'), self.json_data[1]['label']
        )
        self.assertEquals(self.indexer.get_element_value(
            2, 'score'), self.json_data[2]['score']
        )


class BitmapManagerTestCase(TestCase, fake_filesystem_unittest.TestCase):
    def setUp(self):
        self.dataset = Dataset.objects.create(
            label='inria', key='inria',
            metadata='{\"coords_range\": [-21,21]}'
        )
        f1 = GroupFilter.objects.create(
            label='Lab', key='lab', multi=True, dataset=self.dataset)
        f2 = GroupFilter.objects.create(
            label='Year', key='year', multi=True, dataset=self.dataset)
        self.setUpPyfakefs()
        self.fs.create_dir(settings.BITMAPS_DIR)

    def tearDown(self):
        Dataset.objects.all().delete()

    def test_init_without_filters(self):
        dataset = Dataset.objects.create(label='lri', key='lri',
                                         version='1.0.0')
        manager = BitmapManager(dataset)
        out_dir = os.path.join(settings.BITMAPS_DIR, dataset.key)
        self.assertFalse(os.path.isdir(out_dir))
        self.assertFalse(manager.filtering)

    def test_init_creates_dirs(self):
        manager = BitmapManager(self.dataset)
        out_dir = os.path.join(settings.BITMAPS_DIR, self.dataset.key,
                               self.dataset.version)
        self.assertTrue(os.path.isdir(out_dir))
        self.assertTrue(manager.filtering)

    @patch('datasets.indexing.BitmapStore')
    def test_add_point_field_value(self, mock_bs_class):
        mock_store = MagicMock(spec=BitmapStore)
        mock_bs_class.return_value = mock_store

        manager = BitmapManager(self.dataset)
        manager.add_point_field_values(15, {'nature': 'articles',
                                            'label': 'Article',
                                            'year': '2012',
                                            'position': [29.523, -20.527]}
                                       )
        mock_store.add_field_value.assert_called_with(15, 'year', '2012')
        self.assertEquals(manager.indexed_points, [{'index': 15,
                                                    'x_pos': 29.523,
                                                    'y_pos': -20.527,
                                                    'nature': 'articles'}]
                          )

        manager = BitmapManager(self.dataset)
        mock_store.reset_mock()
        manager.add_point_field_values(178, {'nature': 'authors',
                                             'label': 'Jean Michel',
                                             'lab': '12,45,66,89',
                                             'position': [29.523, -20.527]}
                                       )
        calls = [call(178, 'lab', '12'),
                 call(178, 'lab', '45'),
                 call(178, 'lab', '66'),
                 call(178, 'lab', '89')]
        mock_store.add_field_value.assert_has_calls(calls)
        self.assertEquals(manager.indexed_points,
                          [{'index': 178,
                            'x_pos': 29.523,
                            'y_pos': -20.527,
                            'nature': 'authors'}]
                          )

    @patch('datasets.indexing.CoordBitmapStore')
    def test_create_tile_bitmaps(self, mock_cbs):
        manager = BitmapManager(self.dataset)
        mock_store = MagicMock(CoordBitmapStore)
        mock_cbs.return_value = mock_store
        manager.indexed_points = [
            {'index': i, 'x_pos': -17.558, 'y_pos': 51.485} for i in [0, 1, 3]]

        manager.create_tile_bitmaps(8)
        mock_store.create_store.assert_called_with(ANY, [-21, 21], 8)
        out_dir = os.path.join(settings.BITMAPS_DIR, self.dataset.key,
                               self.dataset.version)
        filename = os.path.join(out_dir, COORDS_BITMAPS_FILENAME)
        mock_cbs.save_store.assert_called_with(mock_store, filename)
