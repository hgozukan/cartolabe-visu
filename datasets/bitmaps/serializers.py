from rest_framework import serializers

from datasets.bitmaps.models import Group


class DatasetGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = '__all__'


class FilterValueSerializer(serializers.BaseSerializer):

    def __init__(self, *args, **kwargs):
        super(FilterValueSerializer, self).__init__(*args, **kwargs)

    def to_representation(self, point):
        point_dict = point.to_dict()
        point_dict['id'] = point.meta.id
        return point_dict


class LayerTilesSerializer(serializers.BaseSerializer):
    def __init__(self, *args, **kwargs):
        super(LayerTilesSerializer, self).__init__(*args, **kwargs)

    def to_representation(self, layers):
        output = {}