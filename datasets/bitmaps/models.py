import os
import shutil
import uuid

from django.conf import settings
from django.db import models
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from elasticsearch_dsl import Document, Keyword, Text, Integer, GeoPoint

from datasets.models import Dataset


class GroupFilter(models.Model):
    label = models.CharField(max_length=100, null=False, blank=False)
    key = models.CharField(max_length=100, null=False, blank=False)
    natures = models.CharField(max_length=200, null=True, blank=True)
    STR = 'ST'
    INT = 'IN'
    REF = 'ID'
    DIS = 'DI'
    FILTER_TYPES = ((STR, 'string'), (INT, 'int'),
                    (REF, 'id'), (DIS, 'distance'))
    type = models.CharField(max_length=2, choices=FILTER_TYPES, default=STR, )
    multi = models.BooleanField(default=False)
    dataset = models.ForeignKey(
        Dataset, on_delete=models.CASCADE, related_name='filters')
    min_size = models.IntegerField(null=False, blank=False, default=1)

    def __str__(self):
        return f"{self.dataset} - {self.key}"


class Group(models.Model):
    """
    A model representing a selection of points on a dataset.
    """
    dataset = models.ForeignKey(
        Dataset, on_delete=models.CASCADE, related_name='groups')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    bitmaps = models.CharField(max_length=1000, null=False, blank=False)
    query = models.CharField(max_length=1000, null=True, blank=True)
    version = models.CharField(max_length=100, null=False, blank=False)

    def __repr__(self):
        return (f"<Group {self.uuid} (dataset={dataset.key}, "
                f"version={self.version}, filters={self.bitmaps})>")

    def __str__(self):
        return f"{self.uuid} ({self.bitmaps})"


class FilterValue(Document):
    """
    An ElasticSearch document representing a bitmap indexed value for a field.
    """
    field = Keyword(required=True, multi=False)
    value = Text(required=True)
    bitmap_index = Integer(required=True)
    rank = Integer(required=False)
    location = GeoPoint(required=False)  # Lat,Long GPS position


@receiver(pre_delete, sender=Group)
def group_file_cleanup(sender, instance, *args, **kwargs):
    """
    Delete the cache files associated with a Group instance
    :return:
    """
    group_dir = os.path.join(settings.STATIC_ROOT,
                             f"grouptiles/{instance.uuid}")
    if os.path.isdir(group_dir):
        shutil.rmtree(group_dir, ignore_errors=True)
