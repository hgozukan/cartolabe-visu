import logging
import os
import pickle
import string
from functools import reduce
from operator import ior, iand
from typing import List, Tuple, Dict

import numpy as np
import pandas as pd
from django.conf import settings
from django.core.cache import cache
from elasticsearch.helpers import streaming_bulk
from elasticsearch_dsl import Index, connections
from pyroaring import BitMap
from tqdm import tqdm

from cartolab.timeit import timeit
from datasets.bitmaps.models import FilterValue, Group, GroupFilter
from datasets.tiling import create_tiles_for_x_y, get_coords_range_for_tiles

logger = logging.getLogger(__name__)

BITMAPS_FILENAME = 'bitmaps.pickle'
COORDS_BITMAPS_FILENAME = 'coords_bitmaps.pickle'
POSITIONS_STORE = 'points.feather'
GROUP_FILTER_INDEX_SUFFIX = '-groups'


class BitmapStore(object):
    """
    Convenience class to store BitMaps and Field/Value combinations.
    """

    def __init__(self, dataset, filters, output_dir):
        self.dataset = dataset
        self.filters = filters
        self.output_dir = output_dir
        self.index = {}
        self.bitmaps = []
        self.filter_values = []

    def _get_index_name(self, dataset):
        return (dataset.key + "-" + dataset.version +
                GROUP_FILTER_INDEX_SUFFIX)

    def add_field_value(self, id, field, value):
        """
        Add field/value mapping for point with id.
        :param id: the point's id that is added to the BitMap
        :param field: the field name
        :param value: the field's value
        :return:
        """
        field_index = self.index.setdefault(field, {})
        bitmap = field_index.setdefault(value, BitMap())
        bitmap.add(id)

    def save_bitmaps(self, elmt_field_getter):
        """
        Save the bitmaps and FieldValue objects in ElasticSearch.
        This method should be called once all field/value combinations have
        been added with the `add_field_value` method

        :return:
        """
        index = Index(self._get_index_name(self.dataset))
        index.document(FilterValue)

        # delete the index, ignore if it doesn't exist
        index.delete(ignore=404)
        # create the index in elasticsearch
        index.create()

        logger.debug('Saving FilterValues into elasticsearch...')
        for ok, result in streaming_bulk(
                connections.get_connection(),
                self._gen_filter_values(elmt_field_getter)
        ):
            if not ok:
                logger.error('Failed to save filter value: %r' % result)

        self._store_bitmaps()

    def _gen_filter_values(self, elmt_field_getter):
        for filter in self.filters:
            field = filter.key
            field_index = self.index.get(filter.key, {})
            logger.info('Field %s has %d distinct values.' %
                        (field, len(field_index)))
            count = 0
            for value, bitmap in tqdm(field_index.items(),
                                      desc=f"Saving {field} values"):
                if len(bitmap) >= filter.min_size:
                    bitmap_index = len(self.bitmaps)
                    filter_value = self._format_filter_value(
                        filter, value, bitmap_index, elmt_field_getter
                    )
                    self.bitmaps.append(bitmap)
                    count += 1
                    yield filter_value.to_dict(True)

            logger.info(
                f"Stored {count} bitmaps with min length {filter.min_size}"
            )

    def _format_filter_value(self, filter: GroupFilter, value, bitmap_index,
                             elmt_field_getter):
        index_name = self._get_index_name(self.dataset)

        if filter.type == GroupFilter.INT:
            return FilterValue(
                field=filter.key,
                value=int(value),
                bitmap_index=bitmap_index,
                meta={'index': index_name}
            )
        elif filter.type == GroupFilter.REF:
            rank = int(value)
            return FilterValue(
                field=filter.key,
                value=elmt_field_getter(rank, 'label'),
                bitmap_index=bitmap_index, rank=rank,
                meta={'index': index_name}
            )
        elif filter.type == GroupFilter.DIS:
            rank = int(value)
            fv = FilterValue(
                field=filter.key,
                value=elmt_field_getter(rank, 'label'),
                bitmap_index=bitmap_index, rank=rank,
                meta={'index': index_name}
            )
            loc = elmt_field_getter(rank, 'location')
            if loc is not None and loc != '':
                fv.location = loc
            return fv
        else:
            return FilterValue(
                field=filter.key,
                value=value, bitmap_index=bitmap_index,
                meta={'index': index_name}
            )

    def _store_bitmaps(self):
        bm = BitmapFilesManager(self.output_dir)
        bm.store_bitmaps(self.bitmaps)


class BitmapFilesManager(object):
    MAX_BITMAPS_PER_FILE = 8000
    MAX_FILES_PER_SUBDIR = 100

    def __init__(self, output_dir):
        self.output_dir = output_dir
        self.cache = {}

    def store_bitmaps(self, bitmaps):
        start = 0
        filenames = self._gen_filename()
        while start < len(bitmaps):
            end = start + self.MAX_BITMAPS_PER_FILE
            if end > len(bitmaps):
                end = len(bitmaps)
            with open(next(filenames), 'wb') as f:
                pickle.dump(bitmaps[start:end], f, pickle.HIGHEST_PROTOCOL)
            start = end

    def _gen_filename(self):
        for a in string.ascii_uppercase:
            sub_dir = os.path.join(self.output_dir, '%s%s' % (a, a))
            if not os.path.isdir(sub_dir):
                os.mkdir(sub_dir)

            for i in range(self.MAX_FILES_PER_SUBDIR):
                filename = os.path.join(
                    sub_dir, 'bitmaps_{:02d}.pickle'.format(i))
                yield filename

    def get_bitmap(self, idx):
        filename, sub_idx = self._get_filename_from_index(idx)
        bitmaps = self.cache.get(filename)

        if bitmaps is None:
            with open(filename, 'rb') as f:
                bitmaps = pickle.load(f)
                self.cache[filename] = bitmaps
        return bitmaps[sub_idx]

    def _get_filename_from_index(self, idx):
        bpd = self.MAX_BITMAPS_PER_FILE * self.MAX_FILES_PER_SUBDIR
        dir_idx = idx // bpd
        file_idx = (idx % bpd) // self.MAX_BITMAPS_PER_FILE
        sub_idx = (idx % bpd) % self.MAX_BITMAPS_PER_FILE
        return os.path.join(
            self.output_dir, '%s%s' % (
                string.ascii_uppercase[dir_idx],
                string.ascii_uppercase[dir_idx]
            ),
            'bitmaps_{:02d}.pickle'.format(file_idx)), sub_idx


class CoordBitmapStore(object):
    """
    A convenience class to manage BitMaps for Tile Coordinates
    """

    def __init__(self):
        self.coords_range = None
        self.zoom = 0
        self.bitmaps = []

    def create_store(self, df: pd.DataFrame, coords_range: List[int], zoom: int):
        """
        Use this method to create the bitmaps that contain the list of point
        ids in each tile

        :param df: The dataframe with the list of points & coordinates to index
        :param coords_range: The coords_range parameter associated with the
        dataset
        :param zoom: The zoom level
        :return:
        """
        self.coords_range = coords_range
        self.zoom = zoom
        self.bitmaps = []
        num_tiles = pow(2, zoom)
        with tqdm(total=num_tiles * num_tiles,
                  desc='Processing tiles',
                  unit='tile') as pbar:

            for x in range(num_tiles):
                for y in range(num_tiles):
                    xrange, yrange = get_coords_range_for_tiles(
                        coords_range, x, y, zoom
                    )
                    ids = df[df['x_pos'].between(
                        xrange[0], xrange[1]) & df['y_pos'].between(
                            yrange[0], yrange[1]
                    )
                    ]
                    self.bitmaps.append(BitMap(ids.index.values))
                    pbar.update(1)
        logger.info('Created %d coords bitmaps.' % len(self.bitmaps))

    def get_bitmap_for_tiles(self, x: int, y: int, z: int, xrange: int = 1,
                             yrange: int = 1) -> BitMap:
        """
        Get a BitMap containing the ids of points located in the specified
                             tiles

        """
        (xmin, ymin), (xmax, ymax) = self._get_contained_coords(
            x, y, z, xrange, yrange
        )
        indices = []
        n = pow(2, self.zoom)
        for i in range(xmin, xmax + 1):
            for j in range(ymin, ymax + 1):
                indices.append(i * n + j)

        bm = reduce(ior, [self.bitmaps[k] for k in indices], BitMap())

        return bm

    def _get_contained_coords(self, x: int, y: int, z: int, xrange: int = 1,
                              yrange: int = 1) -> Tuple[
                                  Tuple[int, int], Tuple[int, int]
    ]:
        """
        Given tile coordinates at zoom level z, return the coordinates of tiles
        at zoom level self.zoom that are equivalent (either contained in, or
        containing).

        """
        if z < self.zoom:
            diff = pow(2, self.zoom - z)
            xmin = x * diff
            ymin = y * diff
            xmax = xmin + xrange * diff - 1
            ymax = ymin + yrange * diff - 1
            return (xmin, ymin), (xmax, ymax)
        elif z > self.zoom:
            diff = pow(2, z - self.zoom)
            xmin = x // diff
            ymin = y // diff
            xmax = (x + xrange - 1) // diff
            ymax = (y + yrange - 1) // diff
            return (xmin, ymin), (xmax, ymax)
        else:
            return (x, y), (x + xrange, y + yrange)

    @classmethod
    def save_store(cls, store, filename):
        with open(filename, 'wb') as f:
            # Pickle the 'data' dictionary using the highest protocol
            # available.

            pickle.dump(store, f, pickle.HIGHEST_PROTOCOL)

    @classmethod
    def load_store(cls, filename):
        with open(filename, 'rb') as f:
            return pickle.load(f)


@timeit
def group_tiling_task(group_uuid: str, x: int, y: int, z: int,
                      layers: Dict[str, np.array]):
    """
    Task to create the tiles for a Group.
    :param group_uuid: The group's uuid
    :param x: The starting x tile coordinate
    :param y: The starting y tile coordinate
    :param z: The zoom level
    :param layers: A dict of nature -> array of 0 or 1s representing the tiles
    to create

    :return: The number of tiles created
    """
    group = Group.objects.get(uuid=group_uuid)
    bitmap = get_bitmap_from_group(group)

    if z > 2:
        # If zoom <= 2, assume we'll display the whole map
        shape = next(iter(layers.values())).shape
        coords_bitmap = get_bitmap_for_coords(
            group, x, y, z, shape[0], shape[1])
        bitmap &= coords_bitmap

    if len(bitmap) == 0:
        return layers

    df = get_pandas_df_from_bitmap(group, bitmap)
    coords_range = group.dataset.get_metadata()['coords_range']

    for nature, tiles in layers.items():
        ndf = df[df['nature'] == nature]
        if len(ndf) == 0:
            continue

        zoom_dir = os.path.join(
            settings.STATIC_ROOT, 'grouptiles/{}/{}/'.format(group_uuid,
                                                             nature), str(z))

        os.makedirs(zoom_dir, exist_ok=True)
        create_tiles_for_x_y(
            ndf, 256, z, x, y, tiles.shape[0], tiles.shape[1], zoom_dir,
            coords_range
        )
        layers[nature] = np.ones(tiles.shape)

    return layers


@timeit
def get_bitmap_for_coords(group: Group, x: int, y: int, z: int,
                          nb_tiles_x: int, nb_tiles_y: int) -> BitMap:
    """
    Get a BitMap containing the ids of points located in the specified tiles
    :param group: The group used to get the dataset
    :param x: The starting x tile coordinate
    :param y: The starting y tile coordinate
    :param z: The zoom level
    :param nb_tiles_x: The nb of tiles on the x axis
    :param nb_tiles_y: The nb of tiles on the y axis
    :return:
    """
    work_dir = os.path.join(settings.BITMAPS_DIR,
                            group.dataset.key, group.dataset.version)
    store = CoordBitmapStore.load_store(
        os.path.join(work_dir, COORDS_BITMAPS_FILENAME))
    return store.get_bitmap_for_tiles(x, y, z, nb_tiles_x, nb_tiles_y)


@timeit
def get_bitmap_from_group(group: Group) -> BitMap:
    """
    Get a BitMap with list of ids from a Group object
    :param group:
    :return:
    """
    manager = BitmapFilesManager(os.path.join(
        settings.BITMAPS_DIR, group.dataset.key, group.dataset.version))
    bm = combine_bitmaps(manager, group.bitmaps, ['&', '|'])
    return bm


def combine_bitmaps(manager: BitmapFilesManager, lst: str, op: list) -> BitMap:
    """
    Combine bitmaps by indices. Indices must be specified in the lst variable,
    as a string with combinations of operators specified in op list.
    :param manager:
    :param lst:
    :param op:
    :return:
    """
    if not op:
        return manager.get_bitmap(int(lst))
    bms = [combine_bitmaps(manager, x, op[1:]) for x in lst.split(op[0])]
    if op[0] == '&':
        return reduce(iand, bms)
    else:
        return reduce(ior, bms)


@timeit
def get_pandas_df_from_bitmap(group: Group, bitmap: BitMap):
    """
    Retrive the coordinates of the points whose id is contained in the bitmap
    set.

    :param group: The group to work with
    :param bitmap: A BitMap containing the ids of points to retrieve
    :return: a pandas DF.
    """
    cache_key = group.dataset.key + group.dataset.version
    df = cache.get(cache_key)
    if df is None:
        logger.info('Loading locations for dataset %s.' %
                    group.dataset.key + group.dataset.version)
        work_dir = os.path.join(settings.BITMAPS_DIR,
                                group.dataset.key, group.dataset.version)
        storefile = os.path.join(work_dir, POSITIONS_STORE)
        df = pd.read_feather(storefile)
        df = df.set_index('index')
        cache.set(cache_key, df)
    return df.loc[list(bitmap)]
