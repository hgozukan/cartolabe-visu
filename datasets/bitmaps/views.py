import os
from typing import List, Dict, Tuple

import numpy as np

from django.conf import settings
from django.http import HttpResponseBadRequest
from elasticsearch_dsl import Search
from rest_framework import permissions, exceptions
from rest_framework.generics import get_object_or_404, GenericAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from cartolab.pagination import ElasticLimitOffsetPagination
from datasets.bitmaps.bitmaps import (
    group_tiling_task, GROUP_FILTER_INDEX_SUFFIX
)
from datasets.bitmaps.models import Group, FilterValue
from datasets.bitmaps.serializers import (
    DatasetGroupSerializer, FilterValueSerializer
)
from datasets.models import Dataset


class InvalidGroupValueException(Exception):
    pass


class GetGroupMixing(object):

    def get_group(self, key, version, bitmaps):

        dataset = get_object_or_404(Dataset, key=key)

        dataset.version = version
        group, created = Group.objects.get_or_create(
            dataset=dataset, version=version, bitmaps=bitmaps)

        if created:
            query = self.request.query_params.get('query')
            if not query:
                raise InvalidGroupValueException(
                    'Query must not be empty when creating a new group.')
            group.query = query
            group.save()

        return group


class DatasetGroupDetailView(APIView, GetGroupMixing):
    """
    Get a Group object from database. Group object is looked up by dataset,
    version & bitmaps combination.

    If a Group object for the given dataset, version & bitmaps combination
    does not exist in database, a new one is created.

    It is up to the user to ensure that the bitmaps string used to identify a
    Group is in a coherent order.


    :param key: the dataset key
    :param key: dataset version
    :param bitmaps: (QueryParam) the bitmaps combination string, e.g. 12&26|64
    :param query: (QueryParam) the query string that corresponds to the bitmaps
    , e.g. 12&26|64

    :return: a Group object
    """
    queryset = Group.objects.all()
    permission_classes = (permissions.AllowAny,)

    def get(self, request, key, version):
        try:
            bitmaps = self.request.query_params.get('bitmaps')
            if not bitmaps:
                raise InvalidGroupValueException(
                    'BitMap list must not be empty.'
                )

            group = self.get_group(key, version, bitmaps)
            serializer = DatasetGroupSerializer(group)
            return Response(serializer.data)

        except InvalidGroupValueException as e:
            return HttpResponseBadRequest(content=str(e))


class DatasetGroupByReferenceView(APIView, GetGroupMixing):

    queryset = Group.objects.all()
    permission_classes = (permissions.AllowAny,)

    def get(self, request, key, version):
        try:
            filter_value = self.get_filter_value()

            group = self.get_group(
                key, version, str(filter_value.bitmap_index)
            )

            serializer = DatasetGroupSerializer(group)
            return Response(serializer.data)

        except InvalidGroupValueException as e:
            return HttpResponseBadRequest(content=str(e))

    def get_filter_value(self):
        index_name = (self.kwargs['key'] + '-' + self.kwargs['version']
                      + GROUP_FILTER_INDEX_SUFFIX)

        s = Search(index=index_name, doc_type=FilterValue)

        field = self.request.query_params.get('field')
        rank = self.request.query_params.get('rank')

        if not field or not rank:
            raise InvalidGroupValueException(
                'Field and rank must be specified.'
            )

        s = s.filter('term', field=field)
        s = s.filter('term', rank=rank)

        result = s[0:10].execute()

        if result.hits.total.value > 1:
            raise InvalidGroupValueException(
                'More than one FilterValue with specified rank.'
            )
        elif result.hits.total.value == 0:
            raise InvalidGroupValueException(
                'No FilterValue with specified rank.'
            )

        return result.hits[0]


class GroupTileView(APIView):
    """
    Request tile creation for a Group. All tiles for zoom level z with
    coordinates between (x, y) and (xend, yend) will be created.

    If all tiles already exist for this zoom level and tile interval, tiles are
    not created again.


    :param uuid: the Group uuid
    :param x: the starting x tile number
    :param y: the starting y tile number
    :param z: the zoom level
    :param xend: the ending x tile number
    :param yend: the ending y tile number
    :param layers: (QueryParam) a list of layers to tile for
    :return: a dict of layer -> np.array where each array has a 1 if the tile
    image exists and a 0 otherwise

    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, uuid, z, x, y, xend, yend):
        if x > xend:
            raise exceptions.ParseError('Invalid request. x > xend')
        if y > yend:
            raise exceptions.ParseError('Invalid request. y > yend')

        layers = self.request.query_params.get('layers')
        if not layers:
            return HttpResponseBadRequest(
                content='You must provide at least one layer.'
            )

        to_tile, all_done = check_files(
            uuid, x, y, z, xend, yend, layers.split(',')
        )
        if len(to_tile) == 0:
            return Response(self._build_response_data(all_done))

        tiled = group_tiling_task(uuid, x, y, z, to_tile)
        return Response(self._build_response_data(tiled, all_done))

    def _build_response_data(self, d1, d2=None):
        data = {
            nature: tiles.tolist() for nature, tiles in d1.items()
        }
        if d2 is not None:
            data.update({
                nature: tiles.tolist() for nature, tiles in d2.items()
            })
        return data


def check_files(uuid: str, x: int, y: int, z: int, xend: int, yend: int,
                layers: List[str], ext='.png') -> Tuple[Dict[str, np.array],
                                                        Dict[str, np.array]]:
    to_tile = {}
    all_done = {}
    for layer in layers:

        files = np.ones((xend - x + 1, yend - y + 1))

        for i in range(files.shape[0]):

            for j in range(files.shape[1]):

                filename = os.path.join(
                    settings.STATIC_ROOT,
                    f"grouptiles/{uuid}/{layer}",
                    str(z),
                    str(i + x),
                    f"{j + y}{ext}"
                )
                if not os.path.isfile(filename):
                    files[i, j] = 0

        if np.all(files == 1):
            all_done[layer] = files
        else:
            to_tile[layer] = files
    return to_tile, all_done


class FilterValueList(GenericAPIView):
    """
    Search the GroupFilter values.
    """
    permission_classes = (permissions.AllowAny,)
    pagination_class = ElasticLimitOffsetPagination
    serializer_class = FilterValueSerializer

    def get(self, request, key, version):
        return self.list()

    def get_queryset(self):
        """Get Queryset."""
        index_name = (self.kwargs['key'] + '-' + self.kwargs['version']
                      + GROUP_FILTER_INDEX_SUFFIX)
        queryset = Search(index=index_name, doc_type=FilterValue)

        return queryset

    def list(self):
        queryset = self.get_points_search()

        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)

        return Response(serializer.data)

    def get_points_search(self):
        """
        Filter the queryset based on the query params
        """
        s = self.get_queryset()

        if 'distance' in self.request.query_params:
            s = s.filter(
                'term', field=self.request.query_params.get('filterKey')
            )
            s = s.filter(
                'geo_distance',
                distance=(
                    f"{self.request.query_params.get('distance', '10')}km"
                ),
                location=self.request.query_params.get('location')
            )
        else:
            for field, value in self.request.query_params.items():
                if field == 'bitmap':
                    s = s.filter('term', bitmap_index=value)
                elif field != 'limit':
                    s = s.filter('term', field=field)
                    if value:
                        s = s.query('multi_match', query=value,
                                    fields=['value'], fuzziness='AUTO')

        return s
