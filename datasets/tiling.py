import logging
import os
import pathlib
import shutil

import pandas as pd
from django.conf import settings
from elasticsearch_dsl import Search
from tqdm import tqdm

from datasets.models import Point
from datasets.image import Tiler, create_tiles_for_zoom


class DatasetWorkingTaskException(Exception):
    pass


logger = logging.getLogger(__name__)


class TilesCreator:
    """
    Class to create tiles for a dataset

    Attributes:
        dataset -- The dataset
    """

    def __init__(self, dataset):
        self.dataset = dataset
        self.metadata = self.dataset.get_metadata()
        if not self.metadata:
            self.metadata = {'coords_range': [-105, 105]}

    def create_tiles(self, max_zoom, dump_images=False):
        """
        Creates the tiles for the dataset for each of the zoom levels in
        range(max_zoom)
        :return:
        """
        tiles_dir = os.path.join(
            settings.STATIC_ROOT,
            'tiles/{}/{}'.format(self.dataset.key,
                                 self.dataset.version)
        )
        if os.path.exists(tiles_dir):
            shutil.rmtree(tiles_dir)

        pathlib.Path(tiles_dir).mkdir(parents=True, exist_ok=True)

        df = self.create_dataframe()
        natures = [nature.key
                   for nature in self.dataset.natures.all() if nature.layer]
        for nature in natures:
            sub_dir = os.path.join(tiles_dir, nature)
            os.mkdir(sub_dir)

            with tqdm(total=total_tiles(max_zoom), unit='tile',
                      desc='Creating %s tiles' % nature) as pbar:
                create_tiles_for_zoom(df.loc[df['nature'] == nature], 256,
                                      max_zoom, sub_dir,
                                      self.metadata['coords_range'], pbar,
                                      dump_images)

    def create_dataframe(self):
        search = Search(index=self.dataset.key + "-" + self.dataset.version,
                        doc_type=Point).source(['position', 'score', 'nature'])
        df = pd.DataFrame(
            (d.to_dict() for d in search.scan()),
            columns=['position', 'score', 'nature']
        )
        df['x_pos'], df['y_pos'] = zip(*df.pop('position'))
        return df


def total_tiles(max_zoom):
    total = 0
    for i in range(max_zoom):
        total += pow(4, i)
    return total


def create_tiles_for_x_y(df, tile_size, zoom, x, y, nb_tiles_x, nb_tiles_y,
                         zoom_dir, coords_range):
    img_width = nb_tiles_x * tile_size
    img_height = nb_tiles_y * tile_size
    x_range, y_range = get_coords_range_for_tiles(coords_range, x, y, zoom,
                                                  nb_tiles_x, nb_tiles_y)

    tiler = Tiler(img_width, img_height, x_range, y_range)
    tiler.create_image(df)
    tiler.crop_image(nb_tiles_x, nb_tiles_y, tile_size, zoom_dir, x, y)


def get_coords_range_for_tiles(coords_range, x, y, z, xrange=1, yrange=1):
    """
    Return the coordinates of the points visible in tiles at zoom level z,
    with tile numbers between (x, y) and (x + xrange, y + yrange)
    :param coords_range:
    :param x:
    :param y:
    :param z:
    :param xrange:
    :param yrange:
    :return:
    """
    nb_tiles_z = 2**z
    data_interval = (coords_range[1] - coords_range[0]) / nb_tiles_z
    x_range = (coords_range[0] + (x * data_interval),
               coords_range[0] + ((x + xrange) * data_interval))
    y_range = (coords_range[1] - ((y + yrange) * data_interval),
               coords_range[1] - (y * data_interval))
    return x_range, y_range
