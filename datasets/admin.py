from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe

from datasets.bitmaps.models import Group, GroupFilter
from datasets.models import Dataset, Nature, ExtraField


class EditLinkToInlineObject(object):
    def extra_fields(self, instance):
        url = reverse('admin:%s_%s_change' % (instance._meta.app_label, instance._meta.model_name), args=[instance.pk])
        if instance.pk:
            extra_fields = instance.extra_fields.all()
            link = 'edit'
            if extra_fields:
                link = ','.join(map(lambda x: x.label, extra_fields))
            return mark_safe(u'<a href="{u}">{l}</a>'.format(u=url, l=link))
        else:
            return ''


class ExtraFieldInline(admin.TabularInline):
    model = ExtraField


class NatureInline(EditLinkToInlineObject, admin.TabularInline):
    model = Nature
    readonly_fields = ('extra_fields',)


class GroupFilterInline(admin.TabularInline):
    model = GroupFilter


class DatasetAdmin(admin.ModelAdmin):
    inlines = [NatureInline, GroupFilterInline]


class NatureAdmin(admin.ModelAdmin):
    inlines = [ExtraFieldInline]


admin.site.register(Dataset, DatasetAdmin)
admin.site.register(Nature, NatureAdmin)
admin.site.register(Group)