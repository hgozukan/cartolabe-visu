import json
import logging
import os
from abc import ABC, abstractmethod
from math import ceil

import pandas as pd
from django.conf import settings
from elasticsearch.helpers import streaming_bulk
from elasticsearch_dsl import Index, connections
from tqdm import tqdm

from datasets.bitmaps.bitmaps import (
    BitmapStore, POSITIONS_STORE, CoordBitmapStore, COORDS_BITMAPS_FILENAME
)
from datasets.models import Point

logger = logging.getLogger(__name__)


def import_for_dataset(export_file, dataset):
    _, extension = os.path.splitext(export_file)
    if extension == '.feather':
        importer = ESFeatherImporter(export_file, dataset)
    else:
        importer = ESJsonImporter(export_file, dataset)
    max_coord = importer.save_to_es()
    return max_coord


def remove_index(dataset, version):
    dataset_key = f"{dataset.key}-{version}"
    index = Index(dataset_key)
    # delete the index, ignore if it doesn't exist
    index.delete(ignore=404)

    group_key = f"{dataset_key}-groups"
    index_group = Index(group_key)
    index_group.delete(ignore=404)


class AbstractESImporter(ABC):

    def __init__(self, export_file, dataset):
        self.export_file = export_file
        self.dataset = dataset
        self.max_coord = 0.0
        self.doc_ids = []
        self.natures = [nature for nature in
                        self.dataset.natures.values_list('key', flat=True)]
        self.bitmap_creator = BitmapManager(dataset)

    def _get_index_key(self, dataset):
        return dataset.key + "-" + dataset.version

    def save_to_es(self):
        """
        Saves the points stored in the dataset file into elasticsearch.
        If an index already exists for the same dataset version, it is
        deleted.

        If the dataset to import into has GroupFilters defined, FilterValues
        will be created for each field / value tuple.
        :return:
        """
        index = Index(self._get_index_key(self.dataset))
        index.document(Point)

        # delete the index, ignore if it doesn't exist
        index.delete(ignore=404)
        # create the index in elasticsearch
        index.create()

        self.create_points()
        self.bitmap_creator.save(self.get_element_value)
        self.add_neighbors()
        return ceil(1.0 + self.max_coord)

    def create_points(self):
        count = 0
        logger.debug('%r Saving points into elasticsearch...' % self)

        for ok, result in tqdm(streaming_bulk(connections.get_connection(),
                                              self._gen_points(),
                                              chunk_size=50,
                                              max_retries=20,
                                              initial_backoff=20,
                                              max_backoff=360),
                               unit='pt',
                               desc='Processing points'):
            action, result = result.popitem()
            doc_id = result['_id']
            if not ok:
                logger.error('Failed to %s document %s: %r' %
                             (action, doc_id, result))
            else:
                count += 1
                self.doc_ids.append(doc_id)
        logger.debug(
            '%r Done saving points. %d points saved into elasticsearch.' % (self, count))

    def add_neighbors(self):
        count = 0
        logger.debug('%r Adding neighbors to points...' % self)
        for ok, result in tqdm(streaming_bulk(connections.get_connection(),
                                              self._gen_neighbors_actions(),
                                              chunk_size=50,
                                              max_retries=20,
                                              initial_backoff=20,
                                              max_backoff=360),
                               total=len(self.doc_ids),
                               unit='pt',
                               desc='Updating points'):
            action, result = result.popitem()
            doc_id = result['_id']
            if not ok:
                logger.error('Failed to %s document %s: %r' %
                             (action, doc_id, result))
            else:
                count += 1
        logger.debug(
            '%r Done updating points.%d points saved into elasticsearch.' % (self, count))

    @abstractmethod
    def get_element_value(self, rank, field):
        pass

    @abstractmethod
    def _gen_points(self):
        pass

    @abstractmethod
    def _gen_neighbors_actions(self):
        pass


class ESJsonImporter(AbstractESImporter):
    """
    Imports the data from an export.json file into an ElasticSearch index.

    Attributes:
        export_file -- The data file
        dataset -- The dataset to import to
    """

    def __init__(self, export_file, dataset):
        super().__init__(export_file, dataset)
        with open(self.export_file, 'r') as f:
            self.data = json.load(f)

    def _gen_points(self):
        """
        Go through the json points and generate a Point document.
        """
        for idx, point in enumerate(self.data):
            position = point['position']
            if abs(position[0]) > self.max_coord:
                self.max_coord = abs(position[0])
            if abs(position[1]) > self.max_coord:
                self.max_coord = abs(position[1])

            fields = {
                k: v for k, v in point.items() if
                k not in ['neighbors', 'x', 'y',
                          'Index'] and v is not None and v != ''
            }
            self.bitmap_creator.add_point_field_values(idx, fields)

            point_doc = Point(
                meta={'index': self._get_index_key(self.dataset)}, **fields)
            yield point_doc.to_dict(True)

    def _gen_neighbors_actions(self):
        """
        Go through the json points and generate an update action for neighbors.
        """
        for idx, point in enumerate(self.data):
            if 'neighbors' in point:
                doc_id = self.doc_ids[idx]
                neighbors = []
                for nature in self.natures:
                    if nature in point['neighbors']:
                        neighbors.append({'nature': nature,
                                          'ids': [self.doc_ids[id] for id in point['neighbors'][nature]]})

                if neighbors:
                    action = {
                        '_op_type': 'update',
                        '_index': self._get_index_key(self.dataset),
                        '_id': doc_id,
                        'doc': {'neighbors': neighbors}
                    }

                    yield action

    def get_element_value(self, rank, field):
        try:
            return self.data[rank][field]
        except KeyError:
            return None

    def __repr__(self):
        return '<ESJsonImporter(dataset=%r)>' % self.dataset


class ESFeatherImporter(AbstractESImporter):
    """
    Imports the data from an export.feather file into an ElasticSearch index.

    Attributes:
        export_file -- The data file
        dataset -- The dataset to import to
    """

    def __init__(self, export_file, dataset):
        super().__init__(export_file, dataset)
        self.df = pd.read_feather(self.export_file)

    def _gen_points(self):
        """
        Go through the json points and generate a Point document.
        """
        for pointt in self.df.itertuples():
            point = pointt._asdict()
            position = [point['x'], point['y']]
            point['position'] = position
            if abs(position[0]) > self.max_coord:
                self.max_coord = abs(position[0])
            if abs(position[1]) > self.max_coord:
                self.max_coord = abs(position[1])

            fields = {
                k: v for k, v in point.items()
                if not k.startswith('nn_') and k not in ['neighbors', 'x', 'y', 'Index'] and v is not None and v != ''
            }

            self.bitmap_creator.add_point_field_values(point['Index'], fields)

            point_doc = Point(
                meta={'index': self._get_index_key(self.dataset)}, **fields)
            yield point_doc.to_dict(True)

    def _gen_neighbors_actions(self):
        """
        Go through the json points and generate an update action for neighbors.
        """
        for pointt in self.df.itertuples():
            point = pointt._asdict()
            idx = point['Index']
            doc_id = self.doc_ids[idx]
            neighbors = []
            for nature in self.natures:
                if 'nn_' + nature in point and point['nn_' + nature] is not None:
                    neighbors.append({
                        'nature': nature,
                        'ids': [self.doc_ids[int(id)] for id in point['nn_' + nature].split(',')]
                    })

            if neighbors:
                action = {
                    '_op_type': 'update',
                    '_index': self._get_index_key(self.dataset),
                    '_id': doc_id,
                    'doc': {'neighbors': neighbors}
                }

                yield action

    def get_element_value(self, rank, field):
        return self.df.at[rank, field]

    def __repr__(self):
        return '<ESJsonImporter(dataset=%r)>' % self.dataset


class BitmapManager(object):
    """
    Helper class to manager BitMap creation when importing data.
    """

    def __init__(self, dataset):
        self.dataset = dataset
        filters = dataset.filters.all()

        if not filters:
            logger.info(
                'Dataset does not have group filters. Skipping bitmaps...')
            self.filtering = False
        else:
            self.filtering = True
            self.fields = [filter.key for filter in filters]
            self.indexed_points = []

            if not os.path.isdir(settings.BITMAPS_DIR):
                os.mkdir(settings.BITMAPS_DIR)

            self.out_dir = os.path.join(
                settings.BITMAPS_DIR, dataset.key, dataset.version)
            if not os.path.isdir(self.out_dir):
                os.makedirs(self.out_dir)

            self.store = BitmapStore(dataset, filters, self.out_dir)

    def save(self, get_labels_function):
        """
        Creates a BitMap for all possible values for the fields specified in
        self.fields. Each possible value is also indexed in ElasticSearch with
        the bitmap index.


        Creates an HDF5 pandas dataframe with the positions of all the indexed
        points.

        :return:
        """
        if self.filtering and self.indexed_points:
            self.store.save_bitmaps(get_labels_function)
            self._store_positions()
            self.create_tile_bitmaps(7)

    def add_point_field_values(self, idx, point):
        """
        Check the point for possible filter values. If the point has a field
        that is a filter, add this point

        :param point:
        :return:
        """
        if not self.filtering:
            return

        indexed = False
        for field in self.fields:
            if field in point:
                indexed = True
                values = point[field]
                if not isinstance(values, list):
                    values = point[field].split(',')
                    point[field] = values
                for value in values:
                    value = value.strip()
                    if value is not None and value != '':
                        self.store.add_field_value(idx, field, value)
        if indexed:
            self.indexed_points.append(
                {'index': idx,
                 'x_pos': point['position'][0],
                 'y_pos': point['position'][1],
                 'nature': point['nature']}
            )

    def _store_positions(self):
        df = pd.DataFrame(self.indexed_points).set_index('index')
        df = df[['x_pos', 'y_pos', 'nature']]
        filename = os.path.join(self.out_dir, POSITIONS_STORE)
        df = df.reset_index()
        df.to_feather(filename)

    def create_tile_bitmaps(self, max_zoom):
        """
        :param max_zoom:
        :return:
        """
        logger.info('%r Creating Bitmaps per tiles...' % self)
        coords_range = self.dataset.get_metadata()['coords_range']
        df = pd.DataFrame(self.indexed_points).set_index('index')
        store = CoordBitmapStore()
        store.create_store(df, coords_range, max_zoom)
        filename = os.path.join(self.out_dir, COORDS_BITMAPS_FILENAME)
        CoordBitmapStore.save_store(store, filename)

    def __repr__(self):
        return '<GroupFilterCreator(dataset=%r)>' % self.dataset
