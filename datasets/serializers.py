import json

from rest_framework import serializers

from datasets.models import Dataset, Nature, ExtraField, Point


class JSONSerializerField(serializers.Field):
    """ Serializer for JSONField -- required to make field writable"""

    def to_internal_value(self, data):
        return json.dumps(data)

    def to_representation(self, value):
        return json.loads(value)


class ExtraFieldSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExtraField
        fields = ('label', 'key', 'tooltip')


class NatureSerializer(serializers.ModelSerializer):
    extra_fields = ExtraFieldSerializer(many=True, read_only=True)

    class Meta:
        model = Nature
        fields = '__all__'


class DatasetSerializer(serializers.ModelSerializer):
    metadata = JSONSerializerField()
    private = serializers.BooleanField(read_only=True)
    natures = NatureSerializer(many=True, read_only=True)

    class Meta:
        model = Dataset
        fields = ('id',
                  'key',
                  'label',
                  'description',
                  'about_description',
                  'analysed_on',
                  'tiled_on',
                  'max_zoom',
                  'metadata',
                  'working',
                  'private',
                  'url',
                  'version',
                  'natures',
                  'filters',)
        depth = 1


class PointSerializer(serializers.BaseSerializer):

    def __init__(self, *args, **kwargs):
        self.expand_neighbors = kwargs.pop('expand_neighbors', None)
        super(PointSerializer, self).__init__(*args, **kwargs)

    def to_representation(self, point):
        point_dict = point.to_dict()
        point_dict['id'] = point.meta.id
        if self.expand_neighbors and 'neighbors' in point_dict:
            self.expand_n(point_dict, point.meta.index)
        return point_dict

    def expand_n(self, point_dict, index):
        p_neighbors = None
        for neighbors in point_dict['neighbors']:
            if neighbors['nature'] == self.expand_neighbors:
                ids = [{'_id': id, '_source': {'exclude': ['neighbors']}}
                       for id in neighbors['ids']]
                points = Point.mget(ids, index=index)
                neighbors['neighbors'] = list(
                    map(lambda x: self.to_representation(x), points))
                del neighbors['ids']
                p_neighbors = neighbors
        if p_neighbors:
            point_dict['neighbors'] = [p_neighbors]
