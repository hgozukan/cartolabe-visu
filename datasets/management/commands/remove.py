import os
import shutil
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from datasets.models import Dataset
from django.core.exceptions import ObjectDoesNotExist
from datasets.indexing import remove_index


class Command(BaseCommand):
    help = 'Remove dataset'

    def add_arguments(self, parser):
        parser.add_argument('--dataset',
                            help='The name of the dataset to remove'),
        parser.add_argument('--dataset_version',
                            help=('The dataset version to remove, "all" to'
                                  'remove all versions and dataset fixture.'))

    def handle(self, *args, **options):
        dataset_key = options['dataset']
        version = options['dataset_version']

        if version is None:
            raise CommandError(
                "No version specified, nothing will be removed!"
            )

        try:
            dataset = Dataset.objects.get(key=dataset_key)
            self.remove_dataset(dataset, version)
        except ObjectDoesNotExist:
            raise CommandError("The dataset does not exist in the database.")

    def remove_dataset(self, dataset, version):
        bitmaps_dir = os.path.join(settings.BITMAPS_DIR, dataset.key)
        tiles_dir = os.path.join(settings.STATIC_ROOT, "tiles", dataset.key)

        if version != "all":
            bitmaps_dir = os.path.join(bitmaps_dir, version)
            tiles_dir = os.path.join(tiles_dir, version)

            versions_to_remove = [version]
        else:
            self.stdout.write(
                f"Removing dataset {dataset.key} and all existing versions!"
            )
            versions_to_remove = dataset.get_metadata().get('versions', [])

        # remove bitmaps
        if os.path.exists(bitmaps_dir):
            shutil.rmtree(bitmaps_dir)

        # remove tiles
        if os.path.exists(tiles_dir):
            shutil.rmtree(tiles_dir)

        for version in versions_to_remove:
            self.remove_version(dataset, version)

        # delete dataset fixture as well
        if version == "all":
            dataset.delete()

    def remove_version(self, dataset, version):
        versions = dataset.get_metadata().get('versions', [dataset.version])
        # remove from django database
        if version in versions:
            self.stdout.write(f"Removing {dataset.key} version: {version}!")
            versions.remove(version)
            versions = set(versions)
            dataset.update_metadata({'versions': list(versions)})
            dataset.save()
            self.delete_dataset_groups(dataset, version)

        # remove indices
        remove_index(dataset, version)

    def delete_dataset_groups(self, dataset, version):
        """
        Delete the dataset's groups from the database. Deletes only the groups
        created for dataset version that is being indexed. The pre_delete signal
        will also delete the group's files on disk.
        """

        groups = dataset.groups.filter(version=version)

        if groups is not None:
            for group in groups:
                if group.version == dataset.version:
                    group.delete()
