from django.core.management.base import BaseCommand
from django.utils.timezone import now

from datasets.models import Dataset
from datasets.tiling import TilesCreator


class Command(BaseCommand):
    help = 'Create the tiles for a dataset.'

    def add_arguments(self, parser):
        parser.add_argument('--dataset_key',
                            help='The dataset key to create the tiles for.')
        parser.add_argument('--zoom',
                            help='The max zoom level.')
        parser.add_argument('--dump_images', action='store_true',
                            help='Save large images for debug.')
        parser.add_argument('--dataset_version',
                            help='The dataset version to process.')

    def handle(self, *args, **options):
        dataset_key = options['dataset_key']
        if dataset_key is None:
            dataset_key = self.get_dataset_key()

        version = options['dataset_version']
        dataset = Dataset.objects.get(key=dataset_key)
        if version is None:
            version = dataset.version
        else:
            dataset.version = version

        zoom = options['zoom']
        if zoom is None:
            zoom = dataset.max_zoom
        else:
            zoom = int(zoom)

        dump_images = options['dump_images']

        tiles_creator = TilesCreator(dataset)
        tiles_creator.create_tiles(zoom, dump_images)
        dataset.tiled_on = now()
        dataset.max_zoom = zoom
        dataset.working = False
        dataset.save()

    def get_dataset_key(self):
        keys = list(Dataset.objects.all().values_list('key', flat=True))
        return self.get_input_data('Dataset to create the tiles for ({}): '.format(', '.join(keys)))

    def get_zoom(self):
        return self.get_input_data('Max zoom level ( N <= 9): ')

    def get_input_data(self, message, default=None):
        """
        Override this method if you want to customize data inputs or
        validation exceptions.
        """
        raw_value = input(message)
        if default and raw_value == '':
            raw_value = default

        return raw_value
