django==3.2.11
django-environ==0.8.1
django-cors-headers==3.11.0
djangorestframework==3.12.4
djangorestframework-jwt==1.11.0
download
elasticsearch>=7.0.0,<8.0.0
elasticsearch-dsl>=7.0.0,<8.0.0
fast-histogram
pandas
pyarrow
pyroaring
requests
scikit-image>=0.19.1
tables>=3.5.2
tqdm>=4.32.1
whitenoise>=5.3.0
