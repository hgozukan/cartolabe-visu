from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from .views import PointList, PointNeighborsView

urlpatterns = [
    path('<str:key>/<str:version>/points/',
         PointList.as_view(), name='points'),
    path('<str:key>/<str:version>/points/<str:pid>/<str:nature>/',
         PointNeighborsView.as_view(), name='point-neighbors'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
