import logging
import uuid

import requests
from django.conf import settings
from django.core.cache import caches
from elasticsearch_dsl import Search, A

from datasets.models import Point

logger = logging.getLogger(__name__)


CACHE_OFFSETS_KEY = '{}_OFFSETS'


class CustomSearchManager(object):

    def search(self, dataset_key, query):
        """
        Execute a custom search for the query.
        :return:
        """
        search_id = 'carto-{}'.format(uuid.uuid4())
        data = self._request_position(dataset_key, query)
        if data is not None and 'latent' in data and 'projection' in data:
            self.cache_vector(search_id, data['latent'])
            return self.create_search_point(dataset_key, query, search_id, data['projection'][0][0],
                                            data['projection'][0][1])
        return None

    def get_neighbors(self, dataset_key, search_id, nature, n_neighbors=10):
        latent = self.load_vector_from_cache(search_id)
        data = self._request_neighbors(dataset_key, latent, nature, n_neighbors)

        if data is not None:
            offset = self._get_nature_offsets(dataset_key, nature)
            return [offset + n for n in data['neighbors']]
        return None

    def create_search_point(self, dataset_key, query, search_id, x, y):
        point = Point(meta={'index': dataset_key, 'id': search_id}, position=[x, y], score=0.0, nature='prediction',
                      label=query)
        return point

    def cache_vector(self, search_id, vector):
        cache = caches['search-cache']
        cache.set(search_id, vector)

    def load_vector_from_cache(self, search_id):
        cache = caches['search-cache']
        vector = cache.get(search_id)
        return vector

    def _request_position(self, dataset_key, query):
        url = '{}/{}/position'.format(settings.CUSTOM_SEARCH_URL, dataset_key)
        try:
            r = requests.get(url, params={'query': query}, timeout=5)
            if r.status_code == requests.codes.ok:
                return r.json()
        except BaseException as e:
            logger.warning('An exception occured while trying to request custom search. Please check that the '
                           'custom search service is available. {}'.format(e))
        return None

    def _request_neighbors(self, dataset_key, vector, nature, n_neighbors=None):
        url = '{}/{}/neighbors'.format(settings.CUSTOM_SEARCH_URL, dataset_key)
        payload = {'nature': nature, 'vector': vector}
        if n_neighbors is not None:
            payload['n'] = n_neighbors
        try:
            r = requests.post(url, json=payload, timeout=5)
            if r.status_code == requests.codes.ok:
                return r.json()
        except BaseException as e:
            logger.warning('An exception occured while trying to request custom search. Please check that the '
                           'custom search service is available. {}'.format(e))
        return None

    def _get_nature_offsets(self, dataset_key, nature):
        cache = caches['search-cache']
        offsets = cache.get(CACHE_OFFSETS_KEY.format(dataset_key))

        if offsets is None:
            s = Search(index=dataset_key, doc_type=Point)
            a = A('terms', field='nature')
            a.metric('min_rank', 'min', field='rank')
            s.aggs.bucket('by_natures', a)
            r = s.execute()
            offsets = {item.key: int(item.min_rank.value) for item in r.aggregations.by_natures.buckets}
            logger.info('Computed offsets from ES index: {}'.format(offsets))
            cache.set(CACHE_OFFSETS_KEY.format(dataset_key), offsets)

        return offsets[nature]
