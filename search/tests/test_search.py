from unittest.mock import MagicMock, patch

from django.core.cache import caches
from django.test import TestCase

from datasets.models import Point
from search.search import CustomSearchManager


class CustomSearchManagerTestCase(TestCase):

    def setUp(self) -> None:
        self.key = 'mydataset'
        self.query = 'Very long custom search query'
        self.sid = 'carto-890oiaz79èaze'
        self.cache = caches['search-cache']
        self.manager = CustomSearchManager()

    def test_create_search_point(self):
        x = 6.78
        y = -19.89
        point = self.manager.create_search_point(self.key, self.query, self.sid, x, y)
        self.assertEqual(point.label, self.query)
        self.assertEqual(point.meta.id, self.sid)
        self.assertEqual(point.meta.index, self.key)
        self.assertEqual(point.position[0], x)
        self.assertEqual(point.position[1], y)
        self.assertEqual(point.score, 0.0)

    def test_search_returns_none(self):
        self.manager._request_position = MagicMock(return_value=None)
        res = self.manager.search(self.key, self.query)
        self.assertIsNone(res)
        self.manager._request_position.assert_called_with(self.key, self.query)
        self.manager._request_position = MagicMock(return_value={'error': True})
        res = self.manager.search(self.key, self.query)
        self.assertIsNone(res)

    def test_search_returns_point(self):
        request_res = {'latent': [[0.12, 0.13, 0.15]], 'projection': [[6.78, -12.98]]}
        self.manager._request_position = MagicMock(return_value=request_res)
        res = self.manager.search(self.key, self.query)
        self.manager._request_position.assert_called_with(self.key, self.query)
        self.assertIsInstance(res, Point)
        self.assertEqual(res.position[0], request_res['projection'][0][0])
        self.assertEqual(res.position[1], request_res['projection'][0][1])
        self.assertTrue(res.meta.id.startswith('carto-'))
        self.assertEqual(res.label, self.query)
        self.assertEqual(res.meta.index, self.key)

        vector = self.cache.get(res.meta.id)
        self.assertEqual(vector, request_res['latent'])

    def test_get_neighbors_returns_none(self):
        latent = [[0.12, 0.13, 0.15]]
        nature = 'words'
        self.cache.set(self.sid, latent)
        self.manager._request_neighbors = MagicMock(return_value=None)

        res = self.manager.get_neighbors(self.key, self.sid, nature)
        self.manager._request_neighbors.assert_called_with(self.key, latent, nature, 10)
        self.assertIsNone(res)

    def test_get_neighbors_returns_ok(self):
        latent = [[0.12, 0.13, 0.15]]
        nature = 'words'
        request_res = {'neighbors': [10, 14, 189, 900, 876, 30]}
        self.cache.set(self.sid, latent)
        self.manager._request_neighbors = MagicMock(return_value=request_res)
        self.manager._get_nature_offsets = MagicMock(return_value=166783)

        res = self.manager.get_neighbors(self.key, self.sid, nature)
        self.manager._request_neighbors.assert_called_with(self.key, latent, nature, 10)
        self.manager._get_nature_offsets.assert_called_with(self.key, nature)
        self.assertEquals(res, [x + 166783 for x in request_res['neighbors']])

    @patch('search.search.requests.get')
    def test_request_position(self, mock_get):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json = MagicMock(return_value={'ok': True})
        mock_get.return_value = mock_response

        with self.settings(CUSTOM_SEARCH_URL='http://search.com'):
            res = self.manager._request_position(self.key, self.query)

        mock_get.assert_called_with('http://search.com/mydataset/position', params={'query': self.query}, timeout=5)
        self.assertEquals(res, {'ok': True})

    @patch('search.search.requests.post')
    def test_request_neighbors(self, mock_post):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json = MagicMock(return_value={'ok': True})
        mock_post.return_value = mock_response
        vector = [[0.12, 0.13, 0.15]]
        nature = 'words'

        with self.settings(CUSTOM_SEARCH_URL='http://search.com'):
            res = self.manager._request_neighbors(self.key, vector, nature, 3)

        mock_post.assert_called_with('http://search.com/mydataset/neighbors',
                                     json={'nature': nature, 'vector': vector, 'n': 3}, timeout=5)
        self.assertEquals(res, {'ok': True})
