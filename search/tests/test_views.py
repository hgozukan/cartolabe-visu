import random
import string
import time

from django.urls import reverse
from elasticsearch_dsl import Index
from rest_framework import status
from rest_framework.test import APITestCase

from datasets.models import Dataset, Point


class MockDatasetTestCase(APITestCase):
    dataset = None
    index = None

    @classmethod
    def setUpClass(cls):
        key = 'mockds' + ''.join(random.choices(string.ascii_lowercase, k=5))
        cls.dataset = Dataset.objects.create(label='inria', key=key,
                                             version="1.0.0")
        cls.create_mock_points_for_dataset()

    @classmethod
    def tearDownClass(cls):
        cls.index.delete(ignore=404)

    @classmethod
    def create_mock_points_for_dataset(cls):
        index_name = cls.dataset.key + "-" + cls.dataset.version
        cls.index = Index(index_name)
        cls.index.document(Point)
        cls.index.delete(ignore=404)
        cls.index.create()
        cls.p1 = Point(carto_id=1, position=[-12.34, 45.67], score=10.0,
                       nature='articles', label='Article 1', rank=1)
        cls.p1.save(index=index_name)
        cls.p2 = Point(carto_id=2, position=[34.56, -78.98], score=20.0,
                       nature='articles', label='Article 2', rank=2)
        cls.p2.save(index=index_name)
        cls.p3 = Point(carto_id=3, position=[76.45, -73.48], score=30.0,
                       nature='authors', label='Author 1', rank=3)
        cls.p3.save(index=index_name)
        cls.p4 = Point(carto_id=4, position=[-66.99, -39.38], score=45.0,
                       nature='authors', label='Author 2', rank=4)
        cls.p4.save(index=index_name)
        cls.p5 = Point(carto_id=5, position=[-12.2, 9.8], score=40.0,
                       nature='teams', label='Team 1', rank=5)
        cls.p5.save(index=index_name)
        cls.p6 = Point(carto_id=6, position=[3.3, 14.9], score=50.0,
                       nature='labs', label='Lab 1', rank=6)
        cls.p6.save(index=index_name)

        cls.p1.add_neighbors('articles', [cls.p2.meta.id])
        cls.p1.add_neighbors('authors', [cls.p3.meta.id, cls.p4.meta.id])
        cls.p1.add_neighbors('teams', [cls.p5.meta.id])
        cls.p1.add_neighbors('labs', [cls.p6.meta.id])

        cls.p1.save(index=index_name)

        time.sleep(1)


class PointListTestCase(MockDatasetTestCase):

    def test_point_list_should_be_paginated(self):
        """
        The list of points returned by the view should be paginated.
        :return:
        """
        url = reverse('points', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version
        })
        url += '?limit=2'

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response['Content-Range'], 'items 0-2/6')
        self.assertEqual(len(response.data), 2)

    def test_point_list_should_be_ordered_by_score(self):
        url = reverse('points', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version
        })

        url += '?limit=3'
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)
        self.assertEquals(response.data[0]['label'], 'Lab 1')
        self.assertEquals(response.data[1]['label'], 'Author 2')
        self.assertEquals(response.data[2]['label'], 'Team 1')

    def test_point_list_can_filter_by_nature(self):
        url = reverse('points', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version
        })
        url += '?nature=articles'
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

        url = reverse('points', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version
        })
        url += '?nature=authors'
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

        url = reverse('points', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version
        })
        url += '?nature=teams,labs,articles'
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 4)

    def test_point_list_can_search_in_label(self):
        url = reverse('points', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version
        })
        url += '?search=1'
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 4)

    def test_point_list_can_filter_by_box(self):
        url = reverse('points', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version
        })
        url += '?box=(30.2,-72.0),(80.75,-80.0)'
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_point_list_does_not_include_infos(self):
        """
        The PointListView should only include point infos if neighbors=1
        :return:
        """
        url = reverse('points', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version
        })
        url += '?nature=articles'
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertFalse('neighbors' in response.data[0])
        self.assertFalse('neighbors' in response.data[1])

        url += '&neighbors=1'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertFalse('neighbors' in response.data[0])
        self.assertTrue('neighbors' in response.data[1])
        self.assertEquals(
            response.data[1]['neighbors'][0]['nature'], 'articles')


class PointNeighborsView(MockDatasetTestCase):
    def test_PointNeighborsView(self):
        url = reverse('point-neighbors', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version,
            'pid': self.p1.meta.id,
            'nature': 'authors'
        })
        response = self.client.get(url)

        self.assertEqual(len(response.data), 2)
        self.assertEquals(response.data[0]['label'], 'Author 1')
        self.assertEquals(response.data[1]['label'], 'Author 2')

        url = reverse('point-neighbors', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version,
            'pid': self.p1.meta.id,
            'nature': 'articles'
        })
        response = self.client.get(url)

        self.assertEqual(len(response.data), 1)
        self.assertEquals(response.data[0]['label'], 'Article 2')

        url = reverse('point-neighbors', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version,
            'pid': self.p1.meta.id,
            'nature': 'teams'
        })
        response = self.client.get(url)

        self.assertEqual(len(response.data), 1)
        self.assertEquals(response.data[0]['label'], 'Team 1')

        url = reverse('point-neighbors', kwargs={
            'key': self.dataset.key,
            'version': self.dataset.version,
            'pid': self.p1.meta.id,
            'nature': 'labs'
        })
        response = self.client.get(url)

        self.assertEqual(len(response.data), 1)
        self.assertEquals(response.data[0]['label'], 'Lab 1')
