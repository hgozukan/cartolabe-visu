import { Dataset } from './dataset';

describe('Dataset', () => {

  let dataset: Dataset;
  let other: Dataset;
  beforeEach(() => {
    dataset = new Dataset();
    dataset.id = 1;
    dataset.key = 'inria';
    dataset.label = 'Inria';
    other = new Dataset();
    other.id = 1;
    other.key = 'lri';
  });


  describe('datasetCompare', () => {
    it('should return true if same object', () => {
      expect(Dataset.equal(dataset, dataset)).toBe(true);
      expect(Dataset.equal(null, null)).toBe(true);
      expect(Dataset.equal(dataset, undefined)).toBe(false);
      expect(Dataset.equal(null, dataset)).toBe(false);
    });

    it('should return true if same id', () => {
      expect(Dataset.equal(dataset, other)).toBe(true);
      other.id = 3;
      expect(Dataset.equal(dataset, other)).toBe(false);
    });
  });

});
