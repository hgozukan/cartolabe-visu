import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpRequest } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { DatasetService } from './dataset.service';
import { Dataset, FilterValue } from '../models/dataset';

describe('DatasetService', () => {
    let httpTestingController: HttpTestingController;
    let mockDatasets: Dataset[];
    let service: DatasetService;
    
    beforeEach(() => {
	mockDatasets = [{ id: 1, label: 'Inria', key: 'inria',
			  version: '1.0.0', current: '1.0.0' },
			{ id: 2, label: 'LRI', key: 'lri',
			  version: '1.0.0', current: '1.0.0',
			  analysed_on: '2018-08-01' }] as undefined as Dataset[];
	
	TestBed.configureTestingModule({
	    imports: [
		HttpClientTestingModule
	    ],
	    providers: [DatasetService]
	});
	service = TestBed.get(DatasetService);
	httpTestingController = TestBed.get(HttpTestingController);
    });

    afterEach(() => {
	httpTestingController.verify();
    });
    
    it('should be created', () => {
	expect(service).toBeTruthy();
    });
    
    it('should get Datasets', () => {
	const expectedUrl = environment.apiEndpoint + '/datasets/';
	service.getDatasets().subscribe(
	    results => expect(results).toBe(mockDatasets)
	);
	// Expect one request with an authorization header
	const req = httpTestingController.expectOne(expectedUrl);

	expect(req.request.method).toEqual('GET');
	req.flush(mockDatasets);
    });

    it('should get dataset', () => {
	const expectedUrl = environment.apiEndpoint + '/datasets/hal/1.0.0';
	service.getDataset('hal', '1.0.0').subscribe(
	    result => expect(result).toBe(mockDatasets[1])
	);

	const req = httpTestingController.expectOne(expectedUrl);
	expect(req.request.method).toEqual('GET');
	req.flush(mockDatasets[1]);
    });

    it('should search for filter values', () => {
	let results: FilterValue[];
	service.searchFilterValues(
	    mockDatasets[0], 'struct', 'CNRS', '20'
	).subscribe(values => results = values);
	
	const expectedUrl = environment.apiEndpoint + '/datasets/' +
	    mockDatasets[0].key + '-' + mockDatasets[0].version + '/filtervalues/';
	const req = httpTestingController.expectOne(
	    (r: HttpRequest<any>) => r.url.match(expectedUrl) &&
		r.method === 'GET'
	);
	expect(req.request.urlWithParams).toEqual(
	    expectedUrl + '?struct=CNRS&limit=20'
	);
	const mockValues = [
	    { field: 'struct', value: 'CNRS', bitmap_index: 14,
	      rank: 123, location: '' },
	    { field: 'struct', value: 'CNRS Orsay', bitmap_index: 18,
	      rank: 234, location: '' }
	];
	req.flush(mockValues);
	expect(results).toEqual(mockValues);
    });
});

