import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';
import { Observable, of } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

import { DatasetsStore } from 'src/app/datasets/store';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Dataset } from 'src/app/datasets/models';

@Injectable({
  providedIn: 'root',
})
export class DatasetGuard implements CanActivate {
  constructor(private store: DatasetsStore, private router: Router,
    private authService: AuthService, private toastr: ToastrService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
      const key = route.paramMap.get('key');
      const version = route.paramMap.get('version');

      this.store.setDataset(key, version);

      // If version == 'latest', navigate to latest version of the dataset
      if (version === 'latest') {
	  this.router.navigate(['/map/' + key + '/' + this.store.dataset.current]);
      }
    return of(true);
    // return this.store.dataset.pipe(
    //   switchMap(dataset => this.isDatasetPrivate(dataset, state)),
    // );
  }

  isDatasetPrivate(dataset: Dataset, state: RouterStateSnapshot): Observable<boolean> {
    if (dataset.private && !this.authService.isLoggedIn()) {
      this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
      this.toastr.error('This dataset is private. Please login.');
      return of(false);
    }
    return of(true);
  }
}
