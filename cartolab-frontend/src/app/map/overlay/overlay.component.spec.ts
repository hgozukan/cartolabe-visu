import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MapControlComponent } from 'src/testing/components.stubs';
import { OverlayComponent } from './overlay.component';

describe('OverlayComponent', () => {
  let component: OverlayComponent;
  let fixture: ComponentFixture<OverlayComponent>;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [
        OverlayComponent,
        MapControlComponent
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
