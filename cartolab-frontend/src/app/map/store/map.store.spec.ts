import { TestBed } from '@angular/core/testing';
import { MapStore } from 'src/app/map/store';
import { Point } from '../models';
import { Dataset, SearchFilter } from 'src/app/datasets/models';

describe('MapStore', () => {
  let store: MapStore;
  const natures = [
    {
      id: 101, label: 'Articles', key: 'articles', checked: true, limit: 2, color: '#0288d1', icon: '',
      tooltip: '', layer: true, inactive: false, layer_tooltip: '', extra_fields: []
    },
    {
      id: 102, label: 'Authors', key: 'authors', checked: true, limit: 30, color: '#e64a19', icon: '',
      tooltip: '', layer: true, inactive: false, layer_tooltip: '', extra_fields: []
    },
    {
      id: 103, label: 'Teams', key: 'teams', checked: true, limit: 30, color: '#388e3c', icon: '',
      tooltip: '', layer: false, inactive: false, layer_tooltip: '', extra_fields: []
    },
    {
      id: 104, label: 'Labs', key: 'labs', checked: true, limit: 0, color: '#c67100', icon: '',
      tooltip: '', layer: false, inactive: false, layer_tooltip: '', extra_fields: []
    }
  ];
  const dataset = { id: 1, label: 'Inria', key: 'inria', description: 'Desc', natures: natures } as Dataset;
  let points: Point[];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MapStore,
      ]
    });

    store = TestBed.get(MapStore);
    points = [{ id: '1', score: 10, position: [1.0, 2.0], rank: 0 }, { id: '2', score: 5, position: [-5.2, -3.0], rank: 1 }] as Point[];
  });

  it('should be created', () => {
    expect(store).toBeTruthy();
  });

  describe('addViewBoxPoints', () => {
    it('should add points to labels and publish it', () => {
      let results: Point[];
      store.viewBoxPoints$.subscribe(p => results = p);

      store.addViewBoxPoints(points);
      let set = store.labelPoints;
      expect(set.size()).toBe(2);
      expect(results).toEqual(points);

      const p1 = { id: '3', score: 30, position: [7.2, 0.9] } as Point;
      store.addViewBoxPoints([p1]);
      expect(results).toEqual([p1]);
      set = store.labelPoints;
      expect(set.size()).toBe(3);
    });
  });

  describe('setClusters', () => {
    it('should add points to labels and publish it', () => {
      let results: Point[];
      store.viewBoxPoints$.subscribe(p => results = p);

      store.setClusters(points);
      const set = store.labelPoints;
      expect(set.size()).toBe(2);
      expect(results).toEqual(points);
      expect(store.clusterPoints).toEqual(points);
    });
  });

  describe('clearClusters', () => {
    it('should remove clusters', () => {
      store.setClusters(points);
      expect(store.clusterPoints).toEqual(points);
      store.clearClusters();
      expect(store.clusterPoints).toEqual([]);
    });
  });

  describe('setDataset', () => {
    it('should set the search filters and clear state', () => {
      store.setClusters(points);
      store.addViewBoxPoints(points);
      store.setSearchPoints(points, 'Search title');
      store.setSelectedPoint(points[1]);

      store.setDataset(dataset);

      expect(store.clusterPoints).toEqual([]);
      expect(store.labelPoints.size()).toBe(0);
      let results: Point[];
      store.searchPoints$.subscribe(r => results = r);
      expect(results).toEqual([]);
      let title: string;
      store.searchTitle$.subscribe(t => title = t);
      expect(title).toEqual('');
      expect(store.searchFilters).toEqual([
        new SearchFilter('Articles', 'articles', true),
        new SearchFilter('Authors', 'authors', true),
        new SearchFilter('Teams', 'teams', true),
        new SearchFilter('Labs', 'labs', false)
      ]);
    });
  });

  describe('setSelectedPoint', () => {
    it('should push the selected point', () => {
      let result: Point;
      store.selectedPoint$.subscribe(p => result = p);
      expect(result).toBeNull();
      store.setSelectedPoint(points[0]);
      expect(result).toEqual(points[0]);
    });

    it('should set is_selected on the point', () => {
      store.setSelectedPoint(points[0]);
      expect(points[0].is_selected).toBeTruthy();
      store.setSelectedPoint(points[1]);
      expect(points[0].is_selected).toBeFalsy();
      expect(points[1].is_selected).toBeTruthy();
    });
  });

  describe('labelPoints', () => {
    it('should build a set without duplicates', () => {
      const selectedPoint = { id: '1', score: 10, position: [1.0, 2.0], nature: 'articles' } as Point;
      const searchPoints = [{ id: '1', score: 10, position: [1.0, 2.0], nature: 'articles' },
      { id: '2', score: 5, position: [-5.2, -3.0], nature: 'words' }] as Point[];
      const labelPoints = [{ id: '2', score: 5, position: [-5.2, -3.0], nature: 'words' },
      { id: '3', score: 10, position: [1.0, 2.0], nature: 'articles' }] as Point[];

      store.setSelectedPoint(selectedPoint);
      store.setSearchPoints(searchPoints, 'Search results');
      store.addViewBoxPoints(labelPoints);

      const set = store.labelPoints;
      expect(set.size()).toBe(3);
      set.forEach(p => {
        if (p.id === '1') { expect(p.is_search_result).toBe(false, 'selectedPoint should have been added first'); } else
          if (p.id === '2') { expect(p.is_search_result).toBe(true, 'searchPoints should have been added first'); }
      });
    });
  });

});
