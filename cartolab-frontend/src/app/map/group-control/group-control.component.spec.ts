import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialog } from '@angular/material/dialog';

import { of } from 'rxjs';

import { GroupControlComponent } from './group-control.component';
import { GroupFiltersDialogComponent } from './group-filters/group-filters.component';
import { FilterValue } from 'src/app/datasets/models';
import { MapService } from 'src/app/map/services';
import { MapStore } from 'src/app/map/store';
import { MapServiceStub } from 'src/testing/services.stubs';

describe('GroupControlComponent', () => {
    let component: GroupControlComponent;
    let fixture: ComponentFixture<GroupControlComponent>;
    let dialog: MdDialogMock;
    let mapService: MapService;
    let mapStore: MapStore;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [MatDialogModule],
            declarations: [GroupControlComponent],
            providers: [
                { provide: MapService, useClass: MapServiceStub },
                { provide: MatDialog, useClass: MdDialogMock }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(GroupControlComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        mapStore = TestBed.get(MapStore);
        dialog = TestBed.get(MatDialog);
        mapService = TestBed.get(MapService);
    });


    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('breadcrumbs', () => {
        it('should set correctly', () => {
            const filters = [
		{ field: 'labs', value: 'CNRS', bitmap_index: 14, rank: 23786},
		[
		    { field: 'year', value: '2010', bitmap_index: 12 },
		    { field: 'year', value: '2008', bitmap_index: 10 }
		],
		[
		    { field: 'author', value: 'Jean', bitmap_index: 45, rank: 145 },
		    { field: 'author', value: 'Damien', bitmap_index: 150,
		      rank: 124 }
		]
	    ] as Array<FilterValue | FilterValue[]>;
	    
            mapStore.setGroup([], filters);

            expect(component.breadcrumbs).toEqual('CNRS 2010 2008 Jean Damien');
            expect(component.grouped).toBe(true);

            mapStore.setGroup([], []);
            expect(component.breadcrumbs).toEqual('Filter Map');
            expect(component.grouped).toBe(false);
        });
    });

    describe('clearGroups', () => {
        it('should call clearGroup on mapService', () => {
            const event = jasmine.createSpyObj('event', ['stopPropagation']);
            component.clearGroups(event);

            expect(mapService.clearGroup).toHaveBeenCalled();
            expect(event.stopPropagation).toHaveBeenCalled();
        });
    });

    describe('openDialog', () => {
        it('should open dialog and call groupService.displayGroup if filters were selected', () => {
            spyOn(dialog, 'open').and.callThrough();
            const filters = [
		[
		    { field: 'labs', value: 'CNRS', bitmap_index: 14, rank:
		      23786, location: '' }
		]
	    ];
            dialog.mockFilters = filters;

            component.openDialog();

            expect(dialog.open).toHaveBeenCalledWith(GroupFiltersDialogComponent,
						     {
		panelClass: 'group-filters-dialog',
		minWidth: 400,
		maxWidth: 500
	    });
	    
            expect(mapService.displayGroup).toHaveBeenCalledWith(filters);
        });

        it('should open dialog and call groupService.clearGroups if filters were cleared', () => {
            spyOn(dialog, 'open').and.callThrough();
            const filters = [];
            dialog.mockFilters = filters;

            component.openDialog();

            expect(dialog.open).toHaveBeenCalledWith(GroupFiltersDialogComponent,
						     {
		panelClass: 'group-filters-dialog',
		minWidth: 400,
		maxWidth: 500
	    });
	    
            expect(mapService.clearGroup).toHaveBeenCalled();
        });
    });
});

class MdDialogMock {
    // When the component calls this.dialog.open(...) we'll return an object
    // with an afterClosed method that allows to subscribe to the dialog result observable.

    mockFilters: Array<FilterValue | FilterValue[]>;
    
    open(componentOrTemplateRef, config) {
        return {
            afterClosed: () => of(this.mockFilters)
        };
    }
}

