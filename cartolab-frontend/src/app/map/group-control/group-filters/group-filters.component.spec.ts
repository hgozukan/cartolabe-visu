import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { GroupFilterComponent } from 'src/testing/components.stubs';
import { GroupFiltersComponent } from './group-filters.component';
import { DatasetsStore } from 'src/app/datasets/store';
import { Dataset, FilterValue } from 'src/app/datasets/models';

describe('GroupFiltersComponent', () => {
    let component: GroupFiltersComponent;
    let fixture: ComponentFixture<GroupFiltersComponent>;
    let dsStore: DatasetsStore;
    const datasets = [
	{ id: 1, label: 'Inria', key: 'inria', version:'1.0.0',
	  current: '1.0.0', filters: [] },
	{ id: 2, label: 'Hal', key: 'hal', version:'1.0.0',
	  current: '1.0.0',filters: [
	      { label: 'Year', key: 'year', multi: false,
		type: 'in', natures: '' },
	      { label: 'Labs', key: 'struct', multi: false,
		type: 'st', natures: '' }
	  ]
	}
    ] as Dataset[];

    beforeEach(() => {
	TestBed.configureTestingModule({
	    declarations: [
		GroupFiltersComponent,
		GroupFilterComponent
	    ],
	}).compileComponents();
	dsStore = TestBed.get(DatasetsStore);
	dsStore.setDatasets(datasets);
	dsStore.setDataset(datasets[0].key, datasets[0].version);

	fixture = TestBed.createComponent(GroupFiltersComponent);
	component = fixture.componentInstance;
	fixture.detectChanges();
    });


    it('should create', () => {
	expect(component).toBeTruthy();
	const filterEls = fixture.debugElement.queryAll(By.directive(GroupFilterComponent));
	expect(filterEls.length).toBe(0);
    });

    it('should detect dataset changes', () => {
	dsStore.setDataset(datasets[1].key, datasets[1].version);
	fixture.detectChanges();
	const filterEls = fixture.debugElement.queryAll(By.directive(GroupFilterComponent));
	expect(filterEls.length).toBe(2);
	expect(component.filterComponents.length).toBe(2);
    });
    
    describe('getSelectedFilterValues', () => {
	it('should return an empty list', () => {
	    const values = component.getSelectedFilterValues();
	    expect(values).toEqual([]);
	});

	it('should return an array with the group filter values', () => {
	    dsStore.setDataset(datasets[1].key, datasets[1].version);
	    fixture.detectChanges();
	    const filterValue = { field: 'year', value: '2019', bitmap_index: 1 } as FilterValue;
	    const filterValues = [{ field: 'struct', value: 'CNRS', bitmap_index: 2 },
				  { field: 'struct', value: 'INRIA', bitmap_index: 3 }] as FilterValue[];
	    component.filterComponents.forEach(
		(comp: GroupFilterComponent, index: number) => {
		    comp.filterValue = index === 0 ? filterValue : filterValues;
		}
	    );

	    expect(component.getSelectedFilterValues()).toEqual(
		[filterValue, filterValues]
	    );
	});
    });
});
