import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { of } from 'rxjs';
import { zoomIdentity} from 'd3-zoom';
import { environment } from 'src/environments/environment';
import { MapServiceStub } from 'src/testing/services.stubs';
import { MapTilesComponent } from './map-tiles.component';
import { MapService } from 'src/app/map/services';
import { MapProperties } from 'src/app/map/models';
import { Dataset, Group } from 'src/app/datasets/models';
import { MapStore } from 'src/app/map/store';
import { DatasetsStore } from 'src/app/datasets/store';

describe('MapTilesComponent', () => {
    let component: MapTilesComponent;
    let fixture: ComponentFixture<MapTilesComponent>;
    
    let datasets: Dataset[];
    let mapService: MapService;
    let mapStore: MapStore;
    let dsStore: DatasetsStore;
    let svg: DebugElement;

    beforeEach(() => {
	datasets = [
	    {
		id: 1, label: 'Inria', key: 'inria', version:'1.0.0',
		current: '1.0.0', description: 'Desc',
		natures: [
		    {
			id: 101, label: 'Articles', key: 'articles', checked: true,
			limit: 0, color: 'blue', icon: 'subject', tooltip: '',
			layer: true, inactive: false, layer_tooltip: ''
		    },
		    {
			id: 102, label: 'Authors', key: 'authors', checked: true,
			limit: 0, color: 'red', icon: 'person', tooltip: '',
			layer: true, inactive: false, layer_tooltip: ''
		    }
		]
	    },
	    {
		id: 2, label: 'Hal', key: 'hal', version: '1.0.0',
		current: '1.0.0', description: 'Hal', natures: []
	    }] as Dataset[];
	
	TestBed.configureTestingModule({
	    imports: [MatProgressBarModule],
	    declarations: [MapTilesComponent],
	    providers: [
		{ provide: MapService, useClass: MapServiceStub },
		{ provide: Location, useClass: LocationStub }
	    ]
	}).compileComponents();
	mapService = TestBed.get(MapService);
	mapStore = TestBed.get(MapStore);
	dsStore = TestBed.get(DatasetsStore);
	dsStore.setDatasets(datasets);
	dsStore.setDataset(datasets[0].key, datasets[0].version);

	fixture = TestBed.createComponent(MapTilesComponent);
	component = fixture.componentInstance;
	component.props = { width: 600, height: 600 } as MapProperties;
	fixture.detectChanges();
	svg = fixture.debugElement.query(By.css('svg.tiles-svg'));
    });
    
    it('should create', () => {
	expect(component).toBeTruthy();
    });

    it('should redraw layers on dataset change', () => {
	// const newDataset = { id: 2, label: 'LRI', key: 'lri' } as Dataset;
	spyOn(component, 'drawLayers');
	spyOn(component, 'drawGroupLayers');
	dsStore.setDataset(datasets[0].key, datasets[0].version);
	fixture.detectChanges();
	expect(component.drawLayers).toHaveBeenCalled();
	expect(component.drawGroupLayers).toHaveBeenCalled();
    });

    describe('drawLayers', () => {
	it('should display an svg:g for each layer', () => {
	    component.initSvg();
	    fixture.detectChanges();

	    // Selecting elements within svgs is complicated.
	    // See https://github.com/angular/angular/issues/15164
	    let g = svg.nativeElement.getElementsByClassName('articles-layer');
	    expect(g.length).toBe(1);
	    g = svg.nativeElement.getElementsByClassName('authors-layer');
	    expect(g.length).toBe(1);
	});

	it('should update svg:g when toggling layers', () => {
	    // dsStore.setDataset(datasets[0].key);
	    component.initSvg();
	    dsStore.toggleLayer('articles');
	    fixture.detectChanges();
	    let g = svg.nativeElement.getElementsByClassName('articles-layer');
	    expect(g.length).toBe(0);
	    g = svg.nativeElement.getElementsByClassName('authors-layer');
	    expect(g.length).toBe(1);
	});

	it('should remove layers when dataset has no natures', () => {
	    component.initSvg();
	    dsStore.setDataset(datasets[1].key, datasets[1].version);
	    fixture.detectChanges();
	    const g = svg.nativeElement.getElementsByClassName('authors-layer');
	    expect(g.length).toBe(0);
	});
    });

    describe('zooming', () => {
	it('should scale the tiles', () => {
	    component.initSvg();
	    let t = zoomIdentity;
	    component.zoomed(t);
	    fixture.detectChanges();

	    // Initial zoom should have 4 images in each layer
	    let g = svg.nativeElement.getElementsByClassName('articles-layer');
	    let expectedTileUrl = environment.staticEndpoint + '/tiles/' +
		datasets[0].key + '/' + datasets[0].version +  '/articles/1/';
	    expect(g[0].children.length).toBe(4);
	    expect(g[0].children[0].getAttribute('href')).toEqual(expectedTileUrl + '0/0.png');
	    expect(g[0].children[1].getAttribute('href')).toEqual(expectedTileUrl + '1/0.png');
	    expect(g[0].children[2].getAttribute('href')).toEqual(expectedTileUrl + '0/1.png');
	    expect(g[0].children[3].getAttribute('href')).toEqual(expectedTileUrl + '1/1.png');

	    g = svg.nativeElement.getElementsByClassName('authors-layer');
	    expectedTileUrl = environment.staticEndpoint + '/tiles/' +
		datasets[0].key + '/' + datasets[0].version + '/authors/1/';
	    expect(g[0].children.length).toBe(4);
	    expect(g[0].children[0].getAttribute('href')).toEqual(expectedTileUrl + '0/0.png');
	    expect(g[0].children[1].getAttribute('href')).toEqual(expectedTileUrl + '1/0.png');
	    expect(g[0].children[2].getAttribute('href')).toEqual(expectedTileUrl + '0/1.png');
	    expect(g[0].children[3].getAttribute('href')).toEqual(expectedTileUrl + '1/1.png');

	    t = t.scale(1.5);
	    component.zoomed(t);
	    
	    g = svg.nativeElement.getElementsByClassName('articles-layer');
	    expect(g[0].children.length).toBe(9);
	    g = svg.nativeElement.getElementsByClassName('authors-layer');
	    expect(g[0].children.length).toBe(9);
	});
    });

    describe('groups$', () => {
	const groups: Group[] = [
	    { uuid: 'cd865e3d-c00d-4f3d-a999-342927a6e810',
	      bitmaps: '34|56&23', query: 'RE_lab=12,56&IN_year=2012' },
	    { uuid: '68cb3a16-905e-4b73-ba4e-be7cc25431e6',
	      bitmaps: '8079', query: 'RE_words=56783' },
	];

	beforeEach(() => {
	    component.initSvg();
	    mapStore.setGroup(groups, null);
	    fixture.detectChanges();
	});

	it('should create group layers for each group', () => {
	    // There should be two group layers
	    const g = svg.nativeElement.getElementsByClassName('group-layer');
	    expect(g.length).toBe(2);
	    expect(g[0].getAttribute('id')).toEqual('g' + groups[0].uuid);
	    expect(g[1].getAttribute('id')).toEqual('g' + groups[1].uuid);
	    
	    // Each group layer should have an articles layer and an authors layer
	    let groupTileLayer = g[0].getElementsByClassName('group-tile-layer articles-layer');
	    expect(groupTileLayer.length).toBe(1);
	    groupTileLayer = g[0].getElementsByClassName('group-tile-layer authors-layer');
	    expect(groupTileLayer.length).toBe(1);
	    groupTileLayer = g[1].getElementsByClassName('group-tile-layer articles-layer');
	    expect(groupTileLayer.length).toBe(1);
	    groupTileLayer = g[1].getElementsByClassName('group-tile-layer authors-layer');
	    expect(groupTileLayer.length).toBe(1);
	});

	it('should change the style and visibility of regular tiles', () => {
	    // The tile layers should have low visibility and no filter attribute
	    let tg = svg.nativeElement.getElementsByClassName('tile-layers');
	    expect(tg.length).toBe(1);
	    expect(tg[0].style.opacity).toBe('0.2');
	    tg = tg[0].getElementsByClassName('tile-layer');
	    expect(tg.length).toBe(2);
	    expect(tg[0].getAttribute('filter')).toEqual('');
	    expect(tg[1].getAttribute('filter')).toEqual('');
	});

	it('should remove group layers when groups are cleared', () => {
	    mapStore.clearGroup();
	    fixture.detectChanges();
	    
	    const g = svg.nativeElement.getElementsByClassName('group-layer');
	    expect(g.length).toBe(0);
	    const tg = svg.nativeElement.getElementsByClassName('tile-layers');
	    expect(tg.length).toBe(1);
	    expect(tg[0].style.opacity).toBe('1');
	});
    });
    
    describe('tileGroupLayers', () => {
	const group: Group = { uuid: 'cd865e3d-c00d-4f3d-a999-342927a6e810',
			       bitmaps: '34|56&23',
			       query: 'RE_lab=12,56&IN_year=2012' };

	beforeEach(() => {
	    component.initSvg();
	    mapStore.setGroup([group], null);
	    fixture.detectChanges();
	});

	it('should create tiles', () => {
	    (<jasmine.Spy>mapService.requestGroupTile).and.throwError('Woops');
	    const t = zoomIdentity.translate(-300, -300).scale(4);
	    component.zoomed(t);
	    fixture.detectChanges();
	    
	    // expect 9 tiles at this zoomTransform
	    const g = svg.nativeElement.getElementsByClassName('group-tile-layer articles-layer');
	    const tiles = g[0].getElementsByTagName('image');
	    expect(tiles.length).toBe(9);
	    for (const tile of tiles) {
		expect(tile.getAttribute('width')).toBe('256');
		expect(tile.getAttribute('height')).toBe('256');
	    }
	});
	
	it('should set transform attribute on group-layers', () => {
	    (<jasmine.Spy>mapService.requestGroupTile).and.throwError('Woops');
	    const t = zoomIdentity.translate(-300, -300).scale(4);
	    component.zoomed(t);
	    fixture.detectChanges();
	    
	    let g = svg.nativeElement.getElementsByClassName('group-layer');
	    expect(g.length).toBe(1);
	    expect(g[0].getAttribute('transform')).toEqual('translate(-124,-124) scale(1)');
	    
	    component.zoomed(t.scale(2));
	    g = svg.nativeElement.getElementsByClassName('group-layer');
	    expect(g.length).toBe(1);
	    expect(g[0].getAttribute('id')).toEqual('g' + group.uuid);
	    expect(g[0].getAttribute('transform')).toEqual('translate(52,52) scale(1)');
	});

	it('should debounce the zoom and call getNewTiles', fakeAsync(() => {
	    const mask = { 'articles': [[0, 1., 1.], [1., 0, 1.], [1., 1., 0]],
			   'authors': [[0, 0, 0], [0, 0, 0], [0, 0, 0]] };
	    (<jasmine.Spy>mapService.requestGroupTile).and.returnValue(of(mask));
	    const t = zoomIdentity.translate(-300, -300).scale(4);
	    component.zoomed(t);
	    tick(300);
	    fixture.detectChanges();
	    
	    expect(mapService.requestGroupTile).toHaveBeenCalled();
	    const layers = dsStore.layers.filter(d => !d.inactive);
	    expect(mapService.requestGroupTile).toHaveBeenCalledWith(
		group, 3, 0, 0, 2, 2, layers
	    );
	    expect(component.tiling).toBe(false);

	    // articles images should have been set properly
	    let g = svg.nativeElement.getElementsByClassName(
		'group-tile-layer articles-layer'
	    );
	    let tiles = g[0].getElementsByTagName('image');
	    for (let i = 0; i < 9; i++) {
		const x = i % 3, y = Math.floor(i / 3);
		const m = mask['articles'][x][y];
		if (m === 1.0) {
		    expect(tiles[i].getAttribute('href'))
			.toBe(`http://localhost:8000/static/grouptiles/cd865e3d-c00d-4f3d-a999-342927a6e810/articles/3/${x}/${y}.png`);
		} else {
		    expect(tiles[i].getAttribute('href')).toBe('');
		}
	    }
	    // authors images should all have empty hrefs
	    g = svg.nativeElement.getElementsByClassName('group-tile-layer authors-layer');
	    tiles = g[0].getElementsByTagName('image');
	    for (const tile of tiles) {
		expect(tile.getAttribute('href')).toBe('');
	    }
	}));
    });
    
});

class LocationStub implements Partial<Location> {
    path = () => 'localhost:4200';
}
