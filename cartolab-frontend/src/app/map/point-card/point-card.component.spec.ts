import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip'
import { By } from '@angular/platform-browser';
import { MapServiceStub } from 'src/testing/services.stubs';
import { Point } from 'src/app/map/models';
import { Dataset, GroupFilter } from 'src/app/datasets/models';
import { MapService } from 'src/app/map/services';
import { DatasetsStore } from 'src/app/datasets/store';
import { PointCardComponent } from './point-card.component';


describe('PointCardComponent', () => {
    let component: PointCardComponent;
    let fixture: ComponentFixture<PointCardComponent>;
    let mapService: MapService;
    let store: DatasetsStore;
    const dataset = {
	id: 14, key: 'hal', version: '1.0.0', current: '1.0.0',
	label: 'HAL', description: '',
	natures: [
	    {
		id: 101, label: 'Articles', key: 'articles', checked: true,
		limit: 2, color: '#aa00c7', icon: 'subject', tooltip: '',
		layer: true, inactive: false, layer_tooltip: '', extra_fields: []
	    },
	    {
		id: 102, label: 'Authors', key: 'authors', checked: true,
		limit: 35, color: '#c50e29', icon: 'person', tooltip: '',
		layer: true, inactive: false, layer_tooltip: '', extra_fields: []
	    },
	    {
		id: 103, label: 'Teams', key: 'teams', checked: true,
		limit: 10, color: '#c67100', icon: '', tooltip: '',
		layer: false, inactive: false, layer_tooltip: '', extra_fields: []
	    },
	    {
		id: 104, label: 'Labs', key: 'labs', checked: true,
		limit: 25, color: '#009624', icon: '', tooltip: '',
		layer: false, inactive: false, layer_tooltip: '', extra_fields: []
	    },
	    {
		id: 105, label: 'Words', key: 'words', checked: false,
		limit: 10, color: '#1976d2', icon: '', tooltip: '',
		layer: false, inactive: false, layer_tooltip: '', extra_fields: []
	    }
	],
	filters: [
	    { key: 'labs', label: 'Lab', natures: 'teams,labs' },
	    { key: 'year', label: 'Year', natures: undefined }
	]
    } as Dataset;
    
    const point = {
	id: '42',
	score: 389.2, rank: 1, nature: 'words',
	label: 'algorithm', position: []
    } as Point;

    beforeEach(() => {
	TestBed.configureTestingModule({
	    imports: [
		MatTooltipModule,
		MatDialogModule
	    ],
	    declarations: [PointCardComponent],
	    providers: [
		{ provide: MapService, useClass: MapServiceStub },
	    ]
	}).compileComponents();
	mapService = TestBed.get(MapService);
	store = TestBed.get(DatasetsStore);
	store.setDatasets([dataset]);
	store.setDataset(dataset.key, dataset.version);
	fixture = TestBed.createComponent(PointCardComponent);
	component = fixture.componentInstance;
	component.point = point;
	fixture.detectChanges();
    });

    it('should create', () => {
	expect(component).toBeTruthy();
    });

    it('should display the point label', () => {
	const titleEl = fixture.debugElement.query(By.css('.point-label'));
	const expectedTitle = point.label;
	expect(titleEl.nativeElement.textContent).toContain(expectedTitle);
    });
    
    it('should display the point nature without the s', () => {
	const natureEl = fixture.debugElement.query(By.css('.point-nature'));
	const expectedNature = point.nature.charAt(0).toUpperCase() + point.nature.slice(1, -1);
	expect(natureEl.nativeElement.textContent).toContain(expectedNature);
    });

    it('should display a cluster if point nature is unknown', () => {
	const cluster = { id: '12', label: 'computer science',
			  nature: 'vll_clusters' } as Point;
	component.point = cluster;
	component.ngOnChanges({
	    point: new SimpleChange(point, cluster, false)
	});
	fixture.detectChanges();

	const titleEl = fixture.debugElement.query(By.css('.point-label'));
	expect(titleEl.nativeElement.textContent).toContain(cluster.label);
	const natureEl = fixture.debugElement.query(By.css('.point-nature'));
	expect(natureEl.nativeElement.textContent).toContain('Cluster');
    });

    describe('setGroupFilter', () => {
	it('should set the groupFilter the corresponds to the point nature', () => {
	    expect(component.groupFilter).toBeUndefined();
	    const cnrs = { id: '12', label: 'CNRS', nature: 'labs' } as Point;
	    component.point = cnrs;
	    component.ngOnChanges({
		point: new SimpleChange(point, cnrs, false)
	    });
	    fixture.detectChanges();
	    
	    expect(component.groupFilter).toBe(dataset.filters[0]);
	});

	it('filter button should be visible only when groupFilter is set', () => {
	    let filterBtn = fixture.debugElement.query(By.css('.card-actions .map-filter'));
	    expect(filterBtn).toBeNull();

	    const cnrs = { id: '12', label: 'CNRS', nature: 'labs' } as Point;
	    component.point = cnrs;
	    component.ngOnChanges({
		point: new SimpleChange(point, cnrs, false)
	    });
	    fixture.detectChanges();
	    
	    filterBtn = fixture.debugElement.query(By.css('.card-actions .map-filter'));
	    expect(filterBtn.nativeElement.textContent).toContain('input');
	});
    });

    describe('filterMap', () => {
	it('should call displayGroupForPoint on mapService', () => {
	    const filter = { key: 'labs', label: 'Lab', natures: 'teams,labs' } as GroupFilter;
	    component.groupFilter = filter;
	    component.filterMap();
	    expect(mapService.displayGroupForPoint).toHaveBeenCalledWith(filter, point);
	});
    });
    
});
