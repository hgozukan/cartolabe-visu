import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MapLabelsComponent, MapTilesComponent, PointListComponent, SearchBoxComponent,
  GroupControlComponent
} from 'src/testing/components.stubs';
import { ActivatedRoute, ActivatedRouteStub, Router } from 'src/testing/router.stubs';
import { Dataset } from 'src/app/datasets/models';
import { DatasetsStore } from 'src/app/datasets/store';
import { MapControlComponent } from './map-control.component';

describe('MapControlComponent', () => {
    let component: MapControlComponent;
    let fixture: ComponentFixture<MapControlComponent>;
    let activatedRoute: ActivatedRouteStub;
    let router: Router;
    let dsStore: DatasetsStore;
    const datasets = [{ id: 1, label: 'Inria', key: 'inria', version: '1.0.0',
			current: '1.0.0', description: 'Desc',
			max_zoom: 10, filters: [] }] as Dataset[];

    beforeEach(() => {
	activatedRoute = new ActivatedRouteStub();
	activatedRoute.testParams = { x: 123, y: 456, k: 12 };
	
	TestBed.configureTestingModule({
	    declarations: [
		MapControlComponent,
		MapTilesComponent,
		MapLabelsComponent,
		PointListComponent,
		SearchBoxComponent,
		GroupControlComponent
	    ],
	    providers: [
		{ provide: ActivatedRoute, useValue: activatedRoute },
		{ provide: Router, useValue: jasmine.createSpyObj('Router', ['navigate']) },
	    ]
	}).compileComponents();
	dsStore = TestBed.get(DatasetsStore);
	dsStore.setDatasets(datasets);
	dsStore.setDataset(datasets[0].key, datasets[0].version);
	fixture = TestBed.createComponent(MapControlComponent);
	component = fixture.componentInstance;
	fixture.detectChanges();
	router = TestBed.get(Router);
    });
    
    it('should create', () => {
	expect(component).toBeTruthy();
    });
    
    it('should create initial zoom from url params', () => {
	expect(component.initZoom).toBeDefined();
	expect(component.initZoom.x).toEqual(123);
	expect(component.initZoom.y).toEqual(456);
	expect(component.initZoom.k).toEqual(12);
    });
    
    it('should call initSvg on child components', () => {
	expect(component.tilesComponent.initSvg).toHaveBeenCalled();
	expect(component.labelsComponent.initSvg).toHaveBeenCalled();
    });
    
    it('should set the width and height props', () => {
	expect(component.props.width).toBeDefined();
	expect(component.props.height).toBeDefined();
    });

    describe('zooming', () => {
	it('should propagate zoom events to children', () => {
	    // reset zoom spies
	    (<jasmine.Spy>component.tilesComponent.zoomed).calls.reset();
	    (<jasmine.Spy>component.labelsComponent.zoomed).calls.reset();
	    component.resetZoom(datasets[0]);
	    
	    spyOn(component, 'zoomed');
	    expect(component.tilesComponent.zoomed).toHaveBeenCalled();
	    expect(component.labelsComponent.zoomed).toHaveBeenCalled();
	});
    });

    describe('resetZoom', () => {
	
	it('should be called when dataset changes', (done) => {
	    const spy = spyOn(
		component.zoomContainer, 'call'
	    ).and.returnValue(component.zoomContainer);
	    fixture.whenStable().then(() => { // wait for dataset to be set
		expect(component.maxZoom).toEqual(256);
		expect(
		    component.labelsComponent.getProjection
		).toHaveBeenCalledWith(123, 456);
		expect(spy).toHaveBeenCalledTimes(2);
		done();
	    });
	});
    });

    describe('toggleFullScreen', () => {
	it('should do nothing if dataset is not set', () => {
	    const spy = router.navigate as jasmine.Spy;
	    component.toggleFullScreen();
	    expect(spy.calls.any()).toBe(false);
	});
	
	it('should navigate to overlay view', (done) => {
	    fixture.whenStable().then(() => { // wait for dataset to be set
		const spy = router.navigate as jasmine.Spy;
		component.toggleFullScreen();
		
		expect(spy).toHaveBeenCalled();
		const navArgs = spy.calls.first().args[0];
		// x & y coords are returned by mock getCenterCoordinates from MapLabelsComponent
		// k = 12 is initialZoom from url params
		expect(navArgs).toEqual(
		    [
			'/overlay',
			datasets[0].key,
			datasets[0].version,
			{ x: 10.12, y: 14.36, k: 12 }
		    ]
		);
		done();
	    });
	});
    });
});
