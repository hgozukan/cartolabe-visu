import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { NgSelectModule } from '@ng-select/ng-select';
import { MapServiceStub } from 'src/testing/services.stubs';
import { SearchBoxComponent } from './search-box.component';
import { MapService } from 'src/app/map/services';

describe('SearchBoxComponent', () => {
  let component: SearchBoxComponent;
  let fixture: ComponentFixture<SearchBoxComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        MatIconModule,
        NgSelectModule
      ],
      declarations: [ SearchBoxComponent ],
      providers: [
        { provide: MapService, useClass: MapServiceStub }
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(SearchBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
