import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { MatIconModule } from '@angular/material/icon';

import { PointCardComponent } from 'src/testing/components.stubs';
import { MapServiceStub } from 'src/testing/services.stubs';
import { MapStore } from 'src/app/map/store';
import { Point } from 'src/app/map/models';
import { MapService } from 'src/app/map/services';
import { PointListComponent } from './point-list.component';

describe('PointListComponent', () => {
  let component: PointListComponent;
  let fixture: ComponentFixture<PointListComponent>;
  let mapService: MapService;
  let mapStore: MapStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatIconModule
      ],
      declarations: [
        PointListComponent,
        PointCardComponent
      ],
      providers: [
        { provide: MapService, useClass: MapServiceStub },
      ]
    }).compileComponents();
    mapService = TestBed.get(MapService);
    mapStore = TestBed.get(MapStore);
    fixture = TestBed.createComponent(PointListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onPointCardAction', () => {
    const point = { id: 'ab67q78qs', score: 145, nature: 'authors', label: 'Jean' } as Point;

    it('should be able to target point', () => {
      let selectedPoint: Point;
      component.target.subscribe((p: Point) => selectedPoint = p);
      component.onPointCardAction(point, 'target');

      expect(selectedPoint).toEqual(point);
    });

    it('should be able to clear selected point', () => {
      const spy = spyOn(mapStore, 'setSelectedPoint');
      component.onPointCardAction(point, 'clear');
      expect(spy).toHaveBeenCalledWith(null);
    });

    it('should be able to get point neighbors', () => {
      component.onPointCardAction(point, 'authors');
      expect(mapService.getNeighbors).toHaveBeenCalledWith(point, 'authors');
    });
  });

  describe('searchPoints', () => {
    it('should be displayed in the search results list', () => {
      const points = [
        { id: 'ab67q78qs', score: 145, nature: 'authors', label: 'Jean' },
        { id: 'xu89yt65h', score: 234, nature: 'articles', label: 'Jean\'s article' },
      ] as Point[];
      mapStore.setSearchPoints(points,
        'Nearest teams for Jeans article with very long title that spans for multiple lines and we want to ellipse');

      fixture.detectChanges();

      // expect 2 divs in results-container
      const pointsDe = fixture.debugElement.queryAll(By.css('.point-result'));
      expect(pointsDe.length).toBe(2);
      // expect search title to be displayed
      const titleDe = fixture.debugElement.query(By.css('.search-title-text'));
      expect(titleDe.nativeElement.textContent)
        .toEqual('Nearest teams for Jeans article with very long title that spans for multiple lin...');
      const countDe = fixture.debugElement.query(By.css('.search-title-count'));
      expect(countDe.nativeElement.textContent).toEqual(' (2)');
    });
  });

  describe('selectedPoint', () => {
    it('should be displayed at the top of the results list', () => {
      const point = { id: 'ab67q78qs', score: 145, nature: 'authors', label: 'Jean' } as Point;
      mapStore.setSelectedPoint(point);
      mapStore.setSearchPoints([], '');

      fixture.detectChanges();

      // expect 1 div in results-container
      const pointsDe = fixture.debugElement.queryAll(By.css('.selected-point'));
      expect(pointsDe.length).toBe(1);
    });
  });
});
