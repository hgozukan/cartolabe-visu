import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MapProperties } from 'src/app/map/models';
import { Dataset, FilterValue, GroupFilter } from 'src/app/datasets/models';
import { GroupFiltersDialogData } from 'src/app/map/group-control/group-filters/group-filters.component';
import { Point } from 'src/app/map/models';

@Component({ selector: 'app-map-control', template: '' })
export class MapControlComponent {
  @Input() overlay: boolean;
  targetPoint = jasmine.createSpy('targetPoint');
  onLayerFiltersChanged = jasmine.createSpy('onLayerFiltersChanged');
}

@Component({ selector: 'app-main', template: '<main role="main" class="fill"><router-outlet></router-outlet></main>' })
export class MainComponent {
}

@Component({ selector: 'app-maintenance', template: '' })
export class MaintenanceComponent {
}

@Component({ selector: 'app-dashboard', template: '' })
export class DashboardComponent {
}

@Component({ selector: 'app-about', template: '' })
export class AboutComponent {
}

@Component({ selector: 'app-map-tiles', template: '' })
export class MapTilesComponent {
  @Input() props: MapProperties;
  initSvg = jasmine.createSpy('initSvg').and.returnValue(null);
  zoomed = jasmine.createSpy('zoomed').and.returnValue(null);
}

@Component({ selector: 'app-map-labels', template: '' })
export class MapLabelsComponent {
  @Input() props: MapProperties;
  initSvg = jasmine.createSpy('initSvg').and.returnValue(null);
  zoomed = jasmine.createSpy('zoomed').and.returnValue(null);
  getProjection = jasmine.createSpy('getProjection').and.returnValue([10.12, 14.36]);
  getCenterCoordinates = jasmine.createSpy('getCenterCoordinates').and.returnValue([10.12, 14.36]);
}

@Component({ selector: 'app-point-card', template: '' })
export class PointCardComponent {
  @Input() point: Point;
  @Input() active: boolean;
  @Input() open: boolean;
}

@Component({ selector: 'app-point-list', template: '' })
export class PointListComponent {
  @Output() target = new EventEmitter<Point>();
}

@Component({ selector: 'app-search-box', template: '' })
export class SearchBoxComponent {
}

@Component({ selector: 'app-group-filter', template: '' })
export class GroupFilterComponent {
  @Input() filter: GroupFilter;
  @Input() dataset: Dataset;
  @Input() data: GroupFiltersDialogData;
  filterValue: FilterValue[] | FilterValue;
}

@Component({ selector: 'app-group-control', template: '' })
export class GroupControlComponent {
}

@Component({ selector: 'app-group-filters', template: '' })
export class GroupFiltersComponent {
  getSelectedFilterValues = jasmine.createSpy('getSelectedFilterValues');
}

@Component({ selector: 'app-navbar', template: '' })
export class NavBarStubComponent {

}

@Component({ selector: 'app-footer', template: '' })
export class FooterStubComponent {

}
