export const environment = {
  production: true,
  apiEndpoint: 'https://cartolabe.fr/api/v1',
  staticEndpoint: 'https://cartolabe.fr/static',
  defaultDatasetKey: 'hal',
  contactEmail: 'cartolabe@lisn.upsaclay.fr',
  appendGA: true
};
