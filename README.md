# Cartolabe

Cartolabe is a web-based visualization system for large textual corpora. It has been initially designed to visualize scientific publication data, in particular the contents of the French scientific repository [HAL](https://hal.archives-ouvertes.fr/) containing publications from French public researchers. It has evolved into a more general purpose framework to display large document collections.

Cartolabe visualizes a large textual corpus by projecting it as a 2D map.  It relies on a flexible text analysis pipeline that uses a multidimensional projection (UMAP), but also several data analysis stages to create a usable visualization. Cartolabe is targeted towards an audience of scientists of any domain and assumes no special training on visualization and data analysis.
It is meant to assist scientists explore articles or authors using a zoomable semantic map. Cartolabe is web-based yet scalable to millions of entries, and has been used to explore large corpora such as arXiv (1M entries), the French National Debate (4.3M entries), and Wikipedia (4.5M entries).

See the application [cartolabe live](https://cartolabe.fr).

## Cartolabe-data

This project is the visualization engine of Cartolabe, which relies on preprocessed data. The [cartolabe-data](https://gitlab.inria.fr/cartolabe/cartolabe-data) project can be used to generate such data.

## Project documentation

Full documentation for the project is available on the [GitLab Wiki](https://gitlab.inria.fr/cartolabe/cartolabe-visu/wikis/home).

## Installation

To install the project locally, please see  [GitLab
Wiki](https://gitlab.inria.fr/cartolabe/cartolabe-visu/-/wikis/Setup).

It is possible to run a local version of Cartolabe without installing the
application using Docker images provided in the
[container registry](https://gitlab.inria.fr/cartolabe/cartolabe-visu/container_registry).


## Docker images (Without installation)

The application comprises of 3 Docker images. There are 2 separate Docker images for the
server backend and frontend. In addition, it uses the official [elasticsearch docker
image](https://www.docker.elastic.co/r/elasticsearch/elasticsearch:7.16.2). 


To be able to run the application using Docker images, Docker Engine, Docker
CLI, and Compose plugin should be installed. See
https://docs.docker.com/engine/install/ for installation instructions. 

Docker Engine and Docker CLI are not available for Windows and Mac. [Docker
Desktop](https://docs.docker.com/get-docker/) is an alternative for Windows and
Mac. It is also available for Linux.

### Run Cartolabe from Docker images

- Elasticsearch server image requires the following command to be run (as root) to work properly:

*For Linux machines*

```
sysctl -w vm.max_map_count=262144
```


*For Windows machines*

```
wsl -d docker-desktop
sysctl -w vm.max_map_count=262144
```

The relevant explanation is
[here](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html#docker-prod-prerequisites).


- Download [compose.yaml](https://gitlab.inria.fr/cartolabe/cartolabe-visu/-/raw/master/docker/compose.yaml?inline=false) file and run the
  following commands (as root) from the same directory with the file. (All
  Docker commands should be run as root)

```
docker compose pull 

docker compose up
```

This will download latest Docker images and run 3 containers. When the
containers are up, it will download data dump, index it and create tiles. It
might take a while for the containers to get ready.


After seeing the message:

```
cartolab_backend   | ####################################################################
cartolab_backend   | 
cartolab_backend   | Cartolabe-backend is ready!
cartolab_backend   | 
cartolab_backend   | Server is available at http://localhost:4200/map/lisn
cartolab_backend   | 
cartolab_backend   | ####################################################################

```

the application will be available at [localhost:4200/map/lisn](http://localhost:4200/map/lisn).


The rest API is available at [localhost:8000/api/v1/](http://localhost:8000/api/v1/).


The application displays the map for the [Cartolabe LISN 2000-2022 dataset](https://zenodo.org/record/7323538)
created by extracting the articles published between 2000 and 2022 by the
authors from LISN from the [HAL open archive](https://hal.archives-ouvertes.fr/).

You can also start the containers to run in the background:

```
docker compose up -d
```


To stop the containers run:

```
docker compose stop
```

in the command line.

The containers persist data for the processed datasets in Docker volumes.

### Processing other Cartolabe datasets

[Cartolabe datasets](https://zenodo.org/search?page=1&size=20&q=cartolabe) community on [Zenodo](https://zenodo.org) contains datasets other than **Cartolabe LISN 2000-2022** dataset. 

[datasets/fixtures](datasets/fixtures) directory contains initial data to load
to the database and the dataset dump URLs for each available dataset. All
datasets in Cartolabe datasets community has a corresponding fixture file under
*datasets/fixtures* dicrectory.

If you want to process other datasets in the [datasets/fixtures](datasets/fixtures)
directory, you can do it by modifying `command` field value in `django_server`
section of the compose.yaml file. For example to process `inria` and `ups` datasets,
modify `command` field of compose.yaml file as follows:
  
  ```
  django_server:
    image: "registry.gitlab.inria.fr/cartolabe/cartolabe-visu/cartolabe-visu-backend:latest"
    container_name: cartolab_backend
    restart: always
    command: ["inria", "ups"]
    environment:
      - ELASTIC_HOST=elasticsearch:9200
	  - defaultDatasetKey=inria
    ports:
      - 8000:8000
    depends_on:
      - elasticsearch
    networks:
      cartolabe_net:
    volumes:
      - vlm_backend:/cartolabe-backend/generated
      - vlm_backend_static:/cartolabe-backend/static
      - ./dumps:/cartolabe-backend/dumps
  ```
  
  `inria` and `ups` stand for the **key** field value in
    [inria.json](datasets/fixtures/inria.json) and
    [ups.json](datasets/fixtures/ups.json) files respectively.

The default map to display in the URL when backend gets ready is specified with the `defaultDatasetKey` field. For example if you want to display *inria*'s map
  by default, it should be changed as:
  
  ```
  defaultDatasetKey=inria
  ```
  
Then you can rerun the containers to process the new dataset:

```
docker compose up
```

### Processing a new dataset using Docker images

To process a new dataset that is not part of Cartolabe datasets:

1. Please see the [Datasets Wiki](https://gitlab.inria.fr/cartolabe/cartolabe-visu/-/wikis/Datasets) to
   create a new dataset and the fixture file.
   
   To be able to use the fixture file with Docker images, you should create a
   directory with name `fixtures` in the same directory with compose.yaml
   file. The fixture file should be located in the `fixtures` directory. 
   Then in the compose.yaml file you should add
   `./fixtures/:/cartolabe-backend/datasets/fixtures/` line to the `volumes`
   section of `django_server` section as follows:
   
   
   ```
    volumes:
      - vlm_backend:/cartolabe-backend/generated
      - vlm_backend_static:/cartolabe-backend/static
      - ./dumps:/cartolabe-backend/dumps
      - ./fixtures/:/cartolabe-backend/datasets/fixtures/
   ```
   
2. You should also modify the `command` field value of `django_server` section
   with the dataset name (the fixture file and the `key` field inside the fixture file should be have the dataset name). For the fixture file `fixture_name.json`, command field should be changed as follows:
   
   ```
   command: ["fixture_name"]
   ```

3. *defaultDatasetKey* should also be set:

	```
	defaultDatasetKey=fixture_name
	```
   

### Starting Docker containers without processing a dataset

To start Docker containers to view already processed datasets, remove all
dataset names in the `command` field. Any name that appears at this field will
cause them to be reprocessed upon container restart.


  ```
    command: []
  ```

Then run:

```
docker compose up
```


## About

Cartolabe is a project developped by

* Philippe Caillou (philippe.caillou@lri.fr), Université Paris Saclay, France
* Jonas Renault (jonas.renault@lri.fr), CNRS, France
* Anne-Catherine Letournel (acl@lri.fr), Université Paris Saclay, France
* Michèle Sebag (Michele.Sebag@lri.fr), CNRS, France
* Jean-Daniel Fekete (Jean-Daniel.Fekete@inria.fr), Inria, France
* Hande Gözükan (hande.gozukan@inria.fr), Inria, France
