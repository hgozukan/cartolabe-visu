import logging
import time


timeit_logger = logging.getLogger(__name__)


def timeit(func):
    """This decorator prints the execution time for the decorated function."""

    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        total = end - start
        if total < 1:
            timeit_logger.debug('%r  %2.2f ms' % (func.__name__, total * 1000))
        else:
            timeit_logger.debug('%r  %2.2f s' % (func.__name__, total))
        return result

    return wrapper
