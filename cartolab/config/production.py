"""
Production machine configuration.
"""

from .common_prod import *  # noqa

ALLOWED_HOSTS = ['localhost',
                 'cartolabe.fr',
                 'cartovm.lisn.upsaclay.fr']

INTERNAL_IPS = ['127.0.0.1']

CORS_ORIGIN_WHITELIST = ('http://localhost',
                         'https://cartolabe.fr',
                         'https://www.cartolabe.fr',
                         'http://cartovm.lisn.upsaclay.fr'
                         )
