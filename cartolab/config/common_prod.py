"""
Production Configurations
"""
from .common import *  # noqa

# Site
# https://docs.djangoproject.com/en/2.0/ref/settings/#allowed-hosts

INSTALLED_APPS += ('gunicorn', )

DEPLOYMENT = 'prod'

# set this to 60 seconds and then to 518400 when you can prove it works
# SECURE_HSTS_SECONDS = 60
# SECURE_HSTS_INCLUDE_SUBDOMAINS = env.bool(
#    "DJANGO_SECURE_HSTS_INCLUDE_SUBDOMAINS", default=True)
# SECURE_FRAME_DENY = env.bool("DJANGO_SECURE_FRAME_DENY", default=True)
# SECURE_CONTENT_TYPE_NOSNIFF = env.bool(
#    "DJANGO_SECURE_CONTENT_TYPE_NOSNIFF", default=True)
# SECURE_BROWSER_XSS_FILTER = True
#SESSION_COOKIE_SECURE = True
#SESSION_COOKIE_HTTPONLY = True
#CSRF_COOKIE_SECURE = True
#CSRF_COOKIE_HTTPONLY = False
SECURE_SSL_REDIRECT = env.bool("DJANGO_SECURE_SSL_REDIRECT", default=False)
X_FRAME_OPTIONS = 'DENY'


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/
#STATIC_ROOT = base('prod/static')
STATIC_ROOT = '/var/www/cartolabe/static'
STATICFILES_DIRS = []
STATIC_URL = '/static/'
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# Password Validation
# https://docs.djangoproject.com/en/2.0/topics/auth/passwords/#module-django.contrib.auth.password_validation
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# LOGGING CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#logging
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'django.server': {
            '()': 'django.utils.log.ServerFormatter',
            'format': '[%(server_time)s] %(message)s',
        },
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
        'search': {
            'format': '[%(asctime)s] %(message)s',
        }
    },
    'handlers': {
        'django.server': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'django.server',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'log_file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '/var/log/gunicorn/django.log',
            'maxBytes': 1024 * 1024 * 30,  # 30 mb
            'formatter': 'verbose'
        },
        'search.file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '/var/log/gunicorn/searches.log',
            'maxBytes': 1024 * 1024 * 30,  # 30 mb
            'formatter': 'search'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
        },
        'django.server': {
            'handlers': ['django.server'],
            'level': 'INFO',
            'propagate': False,
        },
        'django.request': {
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': False,
        },
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'INFO'
        },
        'datasets': {
            'handlers': ['log_file'],
            'level': 'DEBUG'
        },
        'datasets.points.views': {
            'handlers': ['search.file'],
            'level': 'DEBUG'
        },
        'cartolab': {
            'handlers': ['log_file'],
            'level': 'DEBUG'
        }
    }
}


# MEDIA_ROOT = '/media/datas/cartolab'

# STATIC_ROOT = '/var/www/cartolab/static/'

# Add hosts that are allowed to do cross-site requests to CORS_ORIGIN_WHITELIST
# or set CORS_ORIGIN_ALLOW_ALL to True to allow all hosts.
CORS_ORIGIN_ALLOW_ALL = False

# CORS_ORIGIN_ALLOW_ALL = True

# List of dataset keys that are private
PRIVATE_DATASET_KEYS = []

CUSTOM_SEARCH_URL = 'http://localhost:7000'
