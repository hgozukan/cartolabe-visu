import datetime

import environ
from os.path import join
from elasticsearch_dsl import connections

env = environ.Env(  # set default values and casting
    DEBUG=(bool, False),
    SECRET_KEY=(str, 'h4@c1x9okapu5^#iurp21i(vn14s5c#1lqx!$k-#^v%rd#rn!b'),
    ELASTIC_HOST=(str, "localhost")
)

# Build paths inside the project like this: base('desired/local/path')
# - the path containing manage.py
#   (e.g. ~/code/cartolab)
base = environ.Path(__file__) - 3  # two folders back (/a/b/ - 2 = /)
BASE_DIR = base()

# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# - the Django project root containing settings.py
# (e.g. ~/code/cartolab/cartolab)
root = environ.Path(__file__) - 2
PROJECT_ROOT = root()
# PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

# environ.Env.read_env(env_file=base('.env'))  # reading .env file

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Third party apps
    'rest_framework',  # utilities for rest apis
    'corsheaders',
    # 'rest_framework.authtoken',  # token authentication
    # 'django_filters',            # for filtering rest endpoints

    # Your apps
    'datasets.apps.DatasetsConfig',
    'search.apps.SearchConfig',
    'users.apps.UsersConfig',
]

# https://docs.djangoproject.com/en/2.0/topics/http/middleware/
MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'cartolab.urls'
SECRET_KEY = env('SECRET_KEY')
WSGI_APPLICATION = 'cartolab.wsgi.application'

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# Email
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

ADMINS = (
    ('Jean-Daniel Fekete', 'Jean-Daniel.Fekete@inria.fr'),
    ('Philippe Caillou', 'caillou@lri.fr'),
    ('Hande Gözükan', 'hande.gozukan@inria.fr'),
)

generated = base + 'generated'
DB_ROOT = generated('db')

# sqlite3
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': join(DB_ROOT, 'cartolab.db'),
    }
}

# General
# APPEND_SLASH = False
TIME_ZONE = 'UTC'
LANGUAGE_CODE = 'fr-fr'
# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True
USE_L10N = True
USE_TZ = True
# LOGIN_REDIRECT_URL = '/'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/
STATIC_ROOT = base('static')
STATICFILES_DIRS = []
STATIC_URL = '/static/'
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# Media files
MEDIA_ROOT = generated('media')
MEDIA_URL = '/media/'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': STATICFILES_DIRS,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# Set DEBUG to False as a default for safety
# https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env('DEBUG')  # False if not in os.environ

# Custom user app
AUTH_USER_MODEL = 'users.User'

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
# SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Custom user app
# AUTH_USER_MODEL = 'users.User'

# Cache
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'cartolab-locmemcache',
        'TIMEOUT': 300,  # Default timeout 5 min
        'OPTIONS': {
            'MAX_ENTRIES': 3,   # Max 3 dfs in memory per thread
            'CULL_FREQUENCY': 0  # Cull everything when max items reached
        }
    },
    'search-cache': {
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
        'LOCATION': 'cartolab-search-cache',
        'TIMEOUT': 3600,  # Default timeout 60 min
    }
}

# Django Rest Framework
REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'cartolab.pagination.ContentRangeHeaderLimitOffsetPagination',
    'PAGE_SIZE': 2000,
    # 'DATETIME_FORMAT': '%Y-%m-%dT%H:%M:%S%z',
    # 'DEFAULT_RENDERER_CLASSES': (
    #     'rest_framework.renderers.JSONRenderer',
    #     'rest_framework.renderers.BrowsableAPIRenderer',
    # ),
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    )
}

JWT_AUTH = {
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=7),
    'JWT_AUTH_HEADER_PREFIX': 'JWT',
}

# Cartolab settings
# The directory where dataset analysis results are stored
DATASETS_DUMP_DIR = base('dumps')

# The directory where bitmaps are stored
BITMAPS_DIR = generated('bitmaps')

# List of dataset keys that are private
PRIVATE_DATASET_KEYS = []  # ['lri']

ELASTIC_HOST = env('ELASTIC_HOST')

# Define a default Elasticsearch client
connections.create_connection(hosts=[ELASTIC_HOST])
