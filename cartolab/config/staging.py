"""
Staging machine configuration.
"""

from .common_prod import *  # noqa

ALLOWED_HOSTS = ['localhost',
                 'cartolabe-beta.lisn.upsaclay.fr',
                 'cartolabe-beta.fr',
                 'cartovm-beta.lisn.upsaclay.fr']

INTERNAL_IPS = ['127.0.0.1']

CORS_ORIGIN_WHITELIST = ('http://localhost',
                         'http://cartolabe-beta.lisn.upsaclay.fr',
                         'https://cartolabe-beta.lisn.upsaclay.fr',
                         'https://www.cartolabe-beta.lisn.upsaclay.fr',
                         'https://cartolabe-beta.fr',
                         'https://www.cartolabe-beta.fr',
                         'http://cartovm-beta.lisn.upsaclay.fr'
                         )
