"""
Local settings

- Run in Debug mode
- Use console backend for emails
- Add Django Debug Toolbar
- Add django-extensions as app
"""
from .common import *  # noqa

# Testing
INSTALLED_APPS = ['whitenoise.runserver_nostatic'] + INSTALLED_APPS

MIDDLEWARE.insert(2, 'whitenoise.middleware.WhiteNoiseMiddleware')

ALLOWED_HOSTS = ["*"]
# Django debug toolbar
INTERNAL_IPS = ['127.0.0.1']

CELERY_ALWAYS_EAGER = True  # run tasks in same thread for development
CELERY_EAGER_PROPAGATES_EXCEPTIONS = True

# Add hosts that are allowed to do cross-site requests to CORS_ORIGIN_WHITELIST
# or set CORS_ORIGIN_ALLOW_ALL to True to allow all hosts.
CORS_ORIGIN_ALLOW_ALL = True

DEPLOYMENT = 'dev'
DEBUG = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/
STATIC_ROOT = base('static')
STATICFILES_DIRS = []
STATIC_URL = '/static/'
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# http://whitenoise.evans.io/en/stable/django.html
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

# Password Validation
# https://docs.djangoproject.com/en/2.0/topics/auth/passwords/#module-django.contrib.auth.password_validation
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# LOGGING
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'django.server': {
            '()': 'django.utils.log.ServerFormatter',
            'format': '[%(server_time)s] %(message)s',
        },
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        }
    },
    'handlers': {
        'django.server': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'django.server',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        }
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
        },
        'django.server': {
            'handlers': ['django.server'],
            'level': 'INFO',
            'propagate': False,
        },
        'django.request': {
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': False,
        },
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'INFO'
        },
        'datasets': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
        'search': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
        'cartolab': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
        'cartodata': {
            'handlers': ['console'],
            'level': 'DEBUG',
        }
    },
}

CUSTOM_SEARCH_URL = 'http://localhost:7000'
